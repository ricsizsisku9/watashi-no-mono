# Watashi no Mono source code

![logo](logo.png "Watashi no Mono")

A mirror of the original source code for Watashi no Mono as released publicly by EpicMeal-Dev through his [Google Drive](https://drive.google.com/file/d/1g568dvIS1XBuWMzaESmcVTcGqMn4efFJ/view) account.

The files provided here in this repository are the same exact files as in his zip archive, except we've added a README and `.gitattributes` file (for Git LFS).

Since this repository is using Git LFS for tracking large files, make sure to [set that up](https://docs.github.com/en/github/managing-large-files/installing-git-large-file-storage) first before cloning this repository. Otherwise, some assets will be missing.
