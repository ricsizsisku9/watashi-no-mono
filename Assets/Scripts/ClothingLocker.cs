﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ClothingLocker : MonoBehaviour {
    public int lockerNumber;
    public ClothingLockerKey keycardScanner;
    public Prompt lockerPrompt;
    public SkinnedMeshRenderer lockerClothingMesh;
    public Mesh[] lockerClothingMeshes;
    public Material[] uniformMaterials;
    public Material[] gymMaterials;
    public clothingType currentClothing = clothingType.Gym;
    public Animator lockerDoorAnimator;
    public bool open = false;

    public PlayerController player;

    public void beginChanging()
    {
        player.stats.canMove = playerMovementMode.cameraOnly;
        player.playAnimation("ChangeClothes", 0.25f);
    }

    public void cancelChanging()
    {
        player.stats.canMove = playerMovementMode.all;
        player.playAnimation(player.moveAnims.idleAnim, 0.25f);
    }

    public void changeClothes(clothingType clothing)
    {
        if (player.stats.bloodiness > 0)
        {
            player.spawnUniform(true);
            currentClothing = clothingType.None;
        }
        else
        {
            currentClothing = player.stats.currentClothing;
        }

        player.stats.currentClothing = clothing;

        Invoke("cancelChanging", 0.1f);
    }

    public void toggleDoor()
    {
        if (!open)
        {
            lockerDoorAnimator.Play("Open");

            lockerPrompt.info = new promptInfo[2];

            lockerPrompt.info[0] = new promptInfo();
            lockerPrompt.info[0].promptText = "Close";
            lockerPrompt.info[0].promptKey = "Interact";

            lockerPrompt.info[1] = new promptInfo();
            lockerPrompt.info[1].promptOffset = new Vector3(0, 0.25f, 0);
            lockerPrompt.info[1].promptText = currentClothing.ToString() + " Uniform";
            lockerPrompt.info[1].promptKey = "Pick up";

            lockerPrompt.info[0].OnFill = new UnityEvent();
            lockerPrompt.info[0].OnFill.AddListener(() => toggleDoor());

            lockerPrompt.info[1].fillSpeed = 0.35f;

            lockerPrompt.info[1].OnFillStart = new UnityEvent();
            lockerPrompt.info[1].OnFillStart.AddListener(() => beginChanging());

            lockerPrompt.info[1].OnFillEnd = new UnityEvent();
            lockerPrompt.info[1].OnFillEnd.AddListener(() => cancelChanging());

            lockerPrompt.info[1].OnFill = new UnityEvent();
            lockerPrompt.info[1].OnFill.AddListener(() => changeClothes(currentClothing));

            player.references.promptManager.clearPrompts();
            player.references.promptManager.lastPrompt = null;

            open = true;
        }
        else
        {
            open = false;
            lockerDoorAnimator.Play("Close");
            lockerPrompt.info[0].promptText = "Open";
            SoundManager.Play("IncineratorClose");
            SoundManager.Play("Print");
            keycardScanner.keycardAnimator.Play("Remove");
            keycardScanner.keycardPrompt.active = true;
            lockerPrompt.active = false;

            keycardScanner.keycardPrompt.info = new promptInfo[1];

            keycardScanner.keycardPrompt.info[0] = new promptInfo();
            keycardScanner.keycardPrompt.info[0].promptText = "Take Student ID";
            keycardScanner.keycardPrompt.info[0].promptKey = "Pick up";

            keycardScanner.keycardPrompt.info[0].OnFill = new UnityEvent();
            keycardScanner.keycardPrompt.info[0].OnFill.AddListener(() => keycardScanner.insertKeyCard0());
        }
    }

    void Update()
    {
        lockerClothingMesh.enabled = currentClothing == clothingType.None ? false : true;
        switch (currentClothing)
        {
            case clothingType.Default:
                lockerClothingMesh.sharedMesh = lockerClothingMeshes[0];
                lockerClothingMesh.materials = uniformMaterials;
                break;
            case clothingType.Gym:
                lockerClothingMesh.sharedMesh = lockerClothingMeshes[1];
                lockerClothingMesh.materials = gymMaterials;
                break;
        }

        if (lockerPrompt.info.Length == 2)
        {
            if (currentClothing != clothingType.None)
            {
                lockerPrompt.info[1].promptText = currentClothing.ToString() + " Uniform";
            }
            else
            {
                lockerPrompt.info = new promptInfo[1];

                lockerPrompt.info[0] = new promptInfo();
                lockerPrompt.info[0].promptText = "Close";
                lockerPrompt.info[0].promptKey = "Interact";

                lockerPrompt.info[0].OnFill = new UnityEvent();
                lockerPrompt.info[0].OnFill.AddListener(() => toggleDoor());
            }
        }
    }
}
