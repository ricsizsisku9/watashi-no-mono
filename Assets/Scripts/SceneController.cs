﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class SceneController : MonoBehaviour {
    public static void loadScene(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
    }

    public static string getSceneName()
    {
        return SceneManager.GetActiveScene().name;
    }

    public static policeEvidenceItem[] loadSceneEvidence()
    {
        string path = Application.streamingAssetsPath + "/Saves/WNM_Save" + PlayerPrefs.GetInt("SaveFileID") + ".wnmsave";
        policeEvidenceItem[] evidence = null;
        saveData SaveData = null;
        if (File.Exists(path))
        {
            StreamReader reader = new StreamReader(path);
            SaveData = JsonUtility.FromJson<saveData>(reader.ReadToEnd());
            reader.Close();

            evidence = SaveData != null ? SaveData.evidence.ToArray() : null;
        }
        return evidence;
    }
}

[System.Serializable] public struct EvidenceArrayWrapper { public policeEvidenceItem[] evidence; }