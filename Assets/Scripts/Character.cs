﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Character : MonoBehaviour {
    [Header("Movement Info")]
    #region ...
    public movementAnimations movementAnimation;
    public movementInfo movementSpeed;
    public Destination currentDestination;
    public Destination lastDestination;
    public bool arrivedAtDestination;
    public bool spotAnimated;
    public bool rotationEnabled = true;
    public bool running;
    public Transform followTarget;
    public followType howToFollow;
    #endregion
    [Header("Character Stats")]
    #region ...
    public characterStatus status;
    public Gender characterGender;
    public string currentFaceAnimation = "";
    public string currentAnimation = "";
    public Transform lookTarget;
    public float Weight = 0.15f;
    #endregion
    [Header("References")]
    #region ...
    [SerializeField] LayerMask visibleLayers;
    [SerializeField] Animator faceAnimator;
    public SkinnedMeshRenderer faceMesh;
    public SkinnedMeshRenderer bodyMesh;
    public AudioSource audioSource;
    public Collider characterCollider;
    public Collider shoveCollider;
    public Rigidbody characterBody;
    public Animator animator;
    public ragdollInfo ragdoll;
    public NavMeshAgent movementAgent;
    #endregion

    // Initiallize
    void Start ()
    {
        if (!ragdoll.ragdolled)
        {
            ragdoll.findRagdollComponents(transform);
            movementAgent.updateRotation = false;
        }
	}

    // Run every frame
    void Update()
    {
        if (rotationEnabled) updateRotation();
        followTransform();
    }

    // Rotate the character to the direction they're moving
    int currentRotationFrame;
    float angle;
    Vector3 toTarget;
    void updateRotation()
    {
        // Rotate to movement if there is no look target
        if (lookTarget == null)
        {
            // Rotate to the direction the student is moving since unity's natural turning speed is extremely slow
            if (movementAgent.velocity.sqrMagnitude > 0.1f)
            {
                Quaternion wantedRotation = Quaternion.LookRotation(movementAgent.velocity.normalized);
                transform.rotation = Quaternion.Lerp(transform.rotation, wantedRotation, Time.deltaTime * 8);
            }
        }
        else
        {
            // Only check the angle every 5 frames
            if (currentRotationFrame > 5)
            {
                toTarget = (lookTarget.position - transform.position).normalized;
                currentRotationFrame = 0;
            }
            else
            {
                currentRotationFrame++;
            }

            // Look at the target if the student can't see them
            if (Vector3.Dot(toTarget, transform.forward) < 0.25f)
            {
                // Student is not looking at the target
                Vector3 wantedDir = lookTarget.position - transform.position;
                wantedDir.y = 0;
                transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.LookRotation(wantedDir), Time.deltaTime * 180);
            }
        }
    }

    public void follow(Transform t, followType type)
    {
        followTarget = t;
        howToFollow = type;
        lastPosition = Vector3.zero;
    }

    Vector3 lastPosition;
    int currentFollowAnimation;
    void followTransform()
    {
        if (followTarget != null)
        {
            movementAgent.stoppingDistance = 1.5f;

            if (Operators.Distance(followTarget.position, lastPosition) > 0.25f)
            {
                NavMeshPath path = new NavMeshPath();
                movementAgent.CalculatePath(followTarget.position, path);
                movementAgent.SetPath(path);
                lastPosition = followTarget.position;
            }

            if (movementAgent.velocity.sqrMagnitude > 0.1f)
            {
                float maxWalkDistance = howToFollow == followType.RunAndWalk ? 3 : howToFollow == followType.WalkOnly ? Mathf.Infinity : 0.1f;
                if (Operators.Distance(transform.position, lastPosition) < maxWalkDistance)
                {
                    // Walk
                    movementAgent.speed = movementSpeed.walkSpeed;
                    playAnimation(movementAnimation.walkAnim, 0.25f);
                }
                else
                {
                    // Run
                    movementAgent.speed = movementSpeed.runSpeed;
                    playAnimation(movementAnimation.runAnim, 0.25f);
                }
            }
            else
            {
                // Idle
                playAnimation(movementAnimation.idleAnim, 0.25f);
            }
        }
    }

    // See if the student can see a certain object
    public bool canSee(Transform target)
    {
        Transform head = ragdoll.Limbs["Head"];
        float viewDistance = 10;
        float angle = Vector3.Angle(head.forward, target.position - head.position);
        float distance = Vector3.Magnitude(target.position - transform.position);
        bool isInFront = angle < 110;

        if (distance < viewDistance && isInFront)
        {
            Vector3 direction = target.position - head.position;
            RaycastHit hit;
            bool raycast = Physics.Raycast(head.position, direction, out hit, distance, visibleLayers);

            if (!raycast)
            {
                Debug.DrawLine(head.position, target.position, Color.green);
                return true;
            }
            else
            {
                Debug.DrawLine(head.position, hit.point, Color.green);
                if (hit.transform.gameObject == target.gameObject)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        else
        {
            return false;
        }
    }

    // Turn to look at a target object
    public void lookAt(Transform target)
    {
        lookTarget = target;
    }

    // Cross fade into an animation if it isn't being played
    public void playAnimation(string animationName, float lerpTime)
    {
        if (currentAnimation != animationName)
        {
            currentAnimation = animationName;
            animator.CrossFadeInFixedTime(animationName, lerpTime);
        }
    }

    // Cross fade into a face animation if it isn't being played
    public void playFaceAnimation(string animationName, float lerpTime)
    {
        if (currentFaceAnimation != animationName)
        {
            currentFaceAnimation = animationName;
            faceAnimator.CrossFadeInFixedTime(animationName, lerpTime);
        }
    }

    // Turn the character into a ragdoll
    public void enableRagdoll()
    {
        ragdoll.enable();
        animator.enabled = false;
        status = characterStatus.Dead;
        characterBody.isKinematic = true;
        characterCollider.enabled = false;
        shoveCollider.enabled = false;
        movementAgent.enabled = false;
        bodyMesh.updateWhenOffscreen = true;
        faceMesh.updateWhenOffscreen = true;
    }

    // Unragdoll the character
    public void disableRagdoll()
    {
        ragdoll.disable();
        animator.enabled = true;
        status = characterStatus.Normal;
        characterBody.isKinematic = false;
        characterCollider.enabled = true;
        shoveCollider.enabled = true;
        movementAgent.enabled = true;
        bodyMesh.updateWhenOffscreen = false;
        faceMesh.updateWhenOffscreen = false;
    }

    // Stop the character's movement
    public void pauseMovement()
    {
        if (movementAgent.isOnNavMesh)
        {
            movementAgent.isStopped = true;
            movementAgent.enabled = false;
            characterBody.isKinematic = true;
        }
    }

    // Make the character continue to the destination they were going to
    public void resumeMovement()
    {
        if (movementAgent.isOnNavMesh)
        {
            movementAgent.isStopped = false;
            movementAgent.enabled = true;
            characterBody.isKinematic = false;
            setDestination(currentDestination, running);
        }
    }

    public void setDestination(Destination target, bool run)
    {
        if (target != null && movementAgent != null)
        {
            lastDestination = currentDestination;
            if (status != characterStatus.Dead)
            {
                running = run;

                // Re-activate movement
                spotAnimated = false;
                characterBody.isKinematic = false;
                movementAgent.enabled = true;
                arrivedAtDestination = false;

                // Return to default blinking if the character isn't scared. 
                // If the character is scared, they would be playing a scared face animation, and returning to a default face would be strange.
                if (status == characterStatus.Normal)
                    faceAnimator.Play("FaceBlink");

                // Calculate a path and follow it to the destination
                NavMeshPath path = new NavMeshPath();
                movementAgent.CalculatePath(target.transform.position, path);
                movementAgent.SetPath(path);

                if (!running)
                {
                    // Walk animation
                    movementAgent.speed = movementSpeed.walkSpeed;
                    animator.CrossFadeInFixedTime(movementAnimation.walkAnim, 0.75f);
                    currentAnimation = movementAnimation.walkAnim;
                }
                else
                {
                    // Sprint if the character is scared, jog if not
                    movementAgent.speed = status == characterStatus.Scared ? movementSpeed.sprintSpeed : movementSpeed.runSpeed;
                    animator.CrossFadeInFixedTime(status == characterStatus.Scared ? movementAnimation.sprintAnim : movementAnimation.runAnim, 0.35f);
                    currentAnimation = status == characterStatus.Scared ? movementAnimation.sprintAnim : movementAnimation.runAnim;
                }

                currentDestination = target;
            }
        }
    }
}
