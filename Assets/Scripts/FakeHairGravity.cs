﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FakeHairGravity : MonoBehaviour
{
    public Transform head;
    public Vector3 offsetRotation;

	void Update ()
    {
        transform.eulerAngles = new Vector3(0, head.transform.eulerAngles.y, 0) + offsetRotation;
	}
}
