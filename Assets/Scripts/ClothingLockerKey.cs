﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ClothingLockerKey : MonoBehaviour {
    public Animator keycardAnimator;
    public Prompt keycardPrompt;
    public ClothingLocker[] unlockableLockers;
    public ClothingLocker currentUnlockedLocker;
    public PlayerController player;

    void Start()
    {
        unlockableLockers = GameObject.FindObjectsOfType<ClothingLocker>();
    }

    void Update()
    {
        if (keycardPrompt.info.Length != player.stolenCards.Count && keycardPrompt.info[0].promptText == "Insert Student ID")
        {
            StartCoroutine(setUpPrompt());
            StartCoroutine(setUpPrompt());
            player.references.promptManager.clearPrompts();
            player.references.promptManager.lastPrompt = null;
        }
    }

    IEnumerator setUpPrompt()
    {
        keycardPrompt.info = new promptInfo[player.stolenCards.Count];

        yield return new WaitForSeconds(0.1f);

        int i = 0;

        foreach (promptInfo info in keycardPrompt.info)
        {
            keycardPrompt.info[i] = new promptInfo();
            if (i == 0)
                keycardPrompt.info[i].promptText = "Insert Student ID";
            else
                keycardPrompt.info[i].promptText = "Insert " + player.stolenCards[i].ownerName + "'s ID";

            switch (i)
            {
                case 0:
                    keycardPrompt.info[i].promptKey = "Interact";
                    break;
                case 1:
                    keycardPrompt.info[i].promptKey = "Pick up";
                    keycardPrompt.info[i].promptOffset = new Vector3(0, 0.25f, 0);
                    break;
                case 2:
                    keycardPrompt.info[i].promptKey = "Attack";
                    keycardPrompt.info[i].promptOffset = new Vector3(0, 0.5f, 0);
                    break;
            }

            keycardPrompt.info[i].OnFill = new UnityEvent();
            int temp = player.stolenCards[i].lockerNumber;
            keycardPrompt.info[i].OnFill.AddListener(delegate { insertKeyCard1(temp); } );
            i++;
        }
    }

    public void insertKeyCard0()
    {
        // Take the card out
        keycardAnimator.Play("Disabled");

        keycardPrompt.info = new promptInfo[1];

        keycardPrompt.info[0] = new promptInfo();
        keycardPrompt.info[0].promptText = "Insert Student ID";
        keycardPrompt.info[0].promptKey = "Interact";

        keycardPrompt.info[0].OnFill = new UnityEvent();
        keycardPrompt.info[0].OnFill.AddListener(() => insertKeyCard1(0));

        player.references.promptManager.lastPrompt = null;
    }

    public void insertKeyCard1(int cardID)
    {
        // Insert the card into the machine
        if (keycardPrompt.info[0].promptText == "Insert Student ID")
        {
            checkCard(cardID);
        }
        else if (keycardPrompt.info[1].promptText == "Insert Student ID")
        {
            checkCard(cardID);
        }
    }

    void checkCard(int cardID)
    {
        foreach (ClothingLocker locker in unlockableLockers)
        {
            if (locker.lockerNumber == cardID)
            {
                keycardAnimator.Play("Insert");
                keycardPrompt.active = false;
                SoundManager.Play("Print");
                currentUnlockedLocker = locker;
                Invoke("unlockLockerDoor", 0.6f);
            }
        }
    }

    public void unlockLockerDoor()
    {
        Prompt lockerPrompt = currentUnlockedLocker.lockerPrompt;

        lockerPrompt.info = new promptInfo[1];

        lockerPrompt.info[0] = new promptInfo();
        lockerPrompt.info[0].promptText = "Open";
        lockerPrompt.info[0].promptKey = "Interact";

        lockerPrompt.info[0].OnFill = new UnityEvent();
        lockerPrompt.info[0].OnFill.AddListener(() => currentUnlockedLocker.toggleDoor());

        lockerPrompt.active = true;
        currentUnlockedLocker.lockerDoorAnimator.Play("Unlock");
        SoundManager.Play("IncineratorOpen");
    }
}
