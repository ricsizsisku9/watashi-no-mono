﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Prompt : MonoBehaviour
{
    [SerializeField] Transform mesh;
    public promptInfo[] info;
    public float maxDistance = 1.25f;
    public bool active = true;

    void Start()
    {
        if (transform.parent.Find("Mesh"))
        {
            mesh = transform.parent.Find("Mesh");
        }
    }

    void Update()
    {
        if (mesh)
        {
            transform.position = mesh.position + new Vector3(0, 0.2f, 0);
        }
    }
}