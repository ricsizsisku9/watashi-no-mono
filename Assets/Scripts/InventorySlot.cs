﻿using UnityEngine;
using UnityEngine.UI;

public class InventorySlot : MonoBehaviour {
    public Image mySprite;
    public Item myItem;
    public int itemID;
}