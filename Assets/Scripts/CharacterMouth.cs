﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMouth : MonoBehaviour {
    public float currentAudioVolume;
    public AudioSource myAudio;
    public SkinnedMeshRenderer faceRenderer;
    public Transform jawBone;
    public float jawRotationMultiplier;
    public float jawRotationSmooth;
    public int mouthFixBlendShape;
    public float mouthBlendShapeMultiplier;
    Vector3 baseJawRotation;
    int dataLength = 1024;
    float[] clipData;
    float[] spectrum;

    void Start () {
        clipData = new float[dataLength];
        baseJawRotation = jawBone.localEulerAngles;
    }
	
	void LateUpdate () {
        // Get the current volume of the playing audio for the jaw bone
        if (myAudio && myAudio.clip != null)
        {
            myAudio.clip.GetData(clipData, myAudio.timeSamples);
            currentAudioVolume = 0f;
            foreach (var sample in clipData)
            {
                currentAudioVolume += Mathf.Abs(sample);
            }
            currentAudioVolume /= dataLength;

            // Get the audio spectrum of the audio for lip sync
            spectrum = new float[256];
            myAudio.GetSpectrumData(spectrum, 0, FFTWindow.Rectangular);

            /* for (int i = 1; i < spectrum.Length - 1; i++)
            {
                Debug.DrawLine(new Vector3(i - 1, spectrum[i] + 10, 0), new Vector3(i, spectrum[i + 1] + 10, 0), Color.red);
                Debug.DrawLine(new Vector3(i - 1, Mathf.Log(spectrum[i - 1]) + 10, 2), new Vector3(i, Mathf.Log(spectrum[i]) + 10, 2), Color.cyan);
                Debug.DrawLine(new Vector3(Mathf.Log(i - 1), spectrum[i - 1] - 10, 1), new Vector3(Mathf.Log(i), spectrum[i] - 10, 1), Color.green);
                Debug.DrawLine(new Vector3(Mathf.Log(i - 1), Mathf.Log(spectrum[i - 1]), 3), new Vector3(Mathf.Log(i), Mathf.Log(spectrum[i]), 3), Color.blue);
            } */

            Vector3 offset = new Vector3(-currentAudioVolume * jawRotationMultiplier * myAudio.volume, 0, 0);
            jawBone.localEulerAngles = Vector3.Lerp(jawBone.localEulerAngles, baseJawRotation + offset, Time.deltaTime * jawRotationSmooth);

            float mouthRotDif = baseJawRotation.x - jawBone.localEulerAngles.x;
            faceRenderer.SetBlendShapeWeight(mouthFixBlendShape, mouthRotDif * mouthBlendShapeMultiplier);
        }
    }
}
