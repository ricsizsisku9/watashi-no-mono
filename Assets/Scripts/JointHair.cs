﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JointHair : MonoBehaviour {
    Rigidbody body;
    [SerializeField]
    Transform player;
    [SerializeField]
    Transform character;

    void Start()
    {
        body = GetComponent<Rigidbody>();
    }

    void Update()
    {
        if (Operators.Distance(player.position, character.position) > 10)
        {
            body.isKinematic = true;
        }
        else
        {
            body.isKinematic = false;
        }
    }
}
