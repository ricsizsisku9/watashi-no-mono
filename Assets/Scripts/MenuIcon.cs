﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuIcon : MonoBehaviour {
    public Color color;
    public Renderer thisRenderer;
    public Text thisText;
    public Animator thisTextAnimator;

    public Transform target;

    // Use this for initialization
    void Start ()
    {
        thisRenderer = GetComponent<Renderer>();
        color = thisRenderer.material.color;
        color.a = 0f;

        if (transform.parent.Find("Name"))
        {
            thisText = transform.parent.Find("Name/Text").GetComponent<Text>();
            thisTextAnimator = thisText.transform.parent.GetComponent<Animator>();
        }
	}
	
	// Update is called once per frame
	void Update ()
    {
		if (Vector3.Magnitude(target.position - transform.position) < 8f)
        {
            if (color.a <= 1f)
            {
                color.a = Mathf.Lerp(color.a, 1f, Time.deltaTime * 5f);
                if (thisText != null)
                {
                    thisTextAnimator.enabled = true;
                }
            }
        }
        if (thisText != null)
        {
            thisText.color = color;
        }
        thisRenderer.material.color = color;
	}
}
