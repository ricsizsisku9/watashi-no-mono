﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BenchScript : MonoBehaviour {
    [SerializeField] Prompt prompt;
    [SerializeField] Transform spot;
    PlayerController player;

    public void sitOnBench()
    {
        if (player == null) player = GameObject.FindObjectOfType<PlayerController>();

        if (player.spotToLerpTo == null)
        {
            if (player.stats.playerState == playerStatus.Nothing)
            {
                player.references.rigidbody.isKinematic = true;
                player.playAnimation(player.moveAnims.sitAnim, 0.25f);
                player.stats.playerState = playerStatus.Sitting;
                player.stats.canMove = playerMovementMode.cameraOnly;
                player.spotToLerpTo = spot;
                prompt.info[0].promptText = "Get Up";
            }
        }
        else
        {
            player.references.rigidbody.isKinematic = false;
            player.playAnimation(player.moveAnims.idleAnim, 0.25f);
            player.stats.playerState = playerStatus.Nothing;
            player.stats.canMove = playerMovementMode.all;
            player.spotToLerpTo = null;
            prompt.info[0].promptText = "Sit down";
        }
    }
}
