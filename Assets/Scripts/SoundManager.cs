﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public static SoundManager activeInstance;
    public AudioClip[] sounds;
    void Start()
    {
        activeInstance = this;
    }

    #region Global AudioClip playing
    // Play the clip globally by name
    public static void Play(string clipName)
    {
        AudioClip toPlay = activeInstance.getSound(clipName);
        if (toPlay == null) return;
        activeInstance.StartCoroutine(activeInstance.PlayGlobally(toPlay, 1));
    }
    // Play the clip globally by name and with specified volume
    public static void Play(string clipName, float volume)
    {
        AudioClip toPlay = activeInstance.getSound(clipName);
        if (toPlay == null) return;
        activeInstance.StartCoroutine(activeInstance.PlayGlobally(toPlay, volume));
    }
    // Play a random clip globally
    public static void PlayRandom(string[] clipNames)
    {
        int randomSoundName = Random.Range(0, clipNames.Length);
        AudioClip toPlay = activeInstance.getSound(clipNames[randomSoundName]);
        if (toPlay == null) return;
        activeInstance.StartCoroutine(activeInstance.PlayGlobally(toPlay, 1));
    }
    // Play a random clip globally by name with specified volume
    public static void PlayRandom(string[] clipNames, float volume)
    {
        int randomSoundName = Random.Range(0, clipNames.Length);
        AudioClip toPlay = activeInstance.getSound(clipNames[randomSoundName]);
        if (toPlay == null) return;
        activeInstance.StartCoroutine(activeInstance.PlayGlobally(toPlay, volume));
    }
    #endregion
    #region AudioSource AudioClip playing
    // Play the clip from an audiosource
    public static void Play(string clipName, AudioSource referenceSource)
    {
        AudioClip toPlay = activeInstance.getSound(clipName);
        if (toPlay == null) return;
        referenceSource.clip = toPlay;
        referenceSource.Play();
    }
    // Play the clip from an audiosource and with specified volume
    public static void Play(string clipName, AudioSource referenceSource, float volume)
    {
        AudioClip toPlay = activeInstance.getSound(clipName);
        if (toPlay == null) return;
        referenceSource.clip = toPlay;
        referenceSource.volume = volume;
        referenceSource.Play();
    }
    // Play a random clip from an audiosource and with specified volume
    public static void PlayRandom(string[] clipNames, AudioSource referenceSource, float volume)
    {
        int randomSoundName = Random.Range(0, clipNames.Length);
        AudioClip toPlay = activeInstance.getSound(clipNames[randomSoundName]);
        if (toPlay == null) return;
        referenceSource.clip = toPlay;
        referenceSource.volume = volume;
        referenceSource.Play();
    }
    // Play a random clip from an audiosource and with specified volume and subtitles
    public static void PlayRandom(string[] clipNames, AudioSource referenceSource, float volume, string[] Subtitles)
    {
        int randomSoundName = Random.Range(0, clipNames.Length);
        AudioClip toPlay = activeInstance.getSound(clipNames[randomSoundName]);
        if (toPlay == null) return;
        referenceSource.clip = toPlay;
        referenceSource.volume = volume;
        referenceSource.Play();
        SubtitlesUI.Get().AddText(Subtitles[randomSoundName], toPlay.length + 1);
    }
    #endregion
    #region Position AudioClip playing
    // Play the clip at a Vector3 coordinate
    public static void Play(string clipName, Vector3 position)
    {
        AudioClip toPlay = activeInstance.getSound(clipName);
        if (toPlay == null) return;
        AudioSource.PlayClipAtPoint(toPlay, position, 1);
    }
    // Play the clip at a Vector3 coordinate and with specified volume
    public static void Play(string clipName, Vector3 position, float volume)
    {
        AudioClip toPlay = activeInstance.getSound(clipName);
        if (toPlay == null) return;
        AudioSource.PlayClipAtPoint(toPlay, position, volume);
    }
    // Play a random clip at a Vector3 coordinate and with specified volume
    public static void PlayRandom(string[] clipNames, Vector3 position, float volume)
    {
        int randomSoundName = Random.Range(0, clipNames.Length);
        AudioClip toPlay = activeInstance.getSound(clipNames[randomSoundName]);
        if (toPlay == null) return;
        AudioSource.PlayClipAtPoint(toPlay, position, volume);
    }
    #endregion

    //Instantiate new AudioSource, play it's clip at specified volume and destroy it after the clip has ended
    public IEnumerator PlayGlobally(AudioClip clip, float volume)
    {
        AudioSource source = new GameObject().AddComponent<AudioSource>();
        source.name = clip.name + " | Live audio instance";
        source.clip = clip;
        source.volume = volume;
        source.spatialBlend = 0;
        source.Play();
        while (source.isPlaying)
        {
            yield return null;
        }
        Destroy(source.gameObject);
    }

    // Search for the AudioClip in sounds array and return it if name matches
    public AudioClip getSound(string name)
    {
        foreach (AudioClip s in sounds)
        {
            if (s != null)
                if (s.name == name)
                    return s;
        }
        return null;
    }
}
