﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.UI;

public class ModManager : MonoBehaviour {
    public static void loadModScript(string modFilePath)
    {
        string path = Application.streamingAssetsPath + "/Packages/" + modFilePath;

        if (File.Exists(path))
        {
            string[] lines = System.IO.File.ReadAllLines(path);
            DebugScript debugManager = GameObject.FindObjectOfType<DebugScript>();
            debugManager.StartCoroutine(debugManager.runModLines(lines));
        }
    }
}
