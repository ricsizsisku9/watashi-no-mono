﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClothingLockerCard : MonoBehaviour {
    public IDCardInfo info;
    public PlayerController player;

    public void pickUp()
    {
        player.stolenCards.Add(info);
        gameObject.SetActive(false);
    }
}

[SerializeField] public class IDCardInfo
{
    public int lockerNumber;
    public string ownerName;
}