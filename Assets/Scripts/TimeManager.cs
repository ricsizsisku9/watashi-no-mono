﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeManager : MonoBehaviour {
    [SerializeField] PlayerController player;
    public float wantedTimeScale = 1;

    void Update()
    {
        if (!player.references.attackManager.killminigame_enabled)
        {
            Time.timeScale = Mathf.Lerp(Time.timeScale, wantedTimeScale, Time.deltaTime * 10);
        }
        else
        {
            Time.timeScale = Mathf.Lerp(Time.timeScale, 0.3f, Time.deltaTime * 10);
        }
    }
}
