﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SightUI : MonoBehaviour {
    [SerializeField] PoliceManager policeManager;
    [SerializeField] PlayerController player;
    [SerializeField] Image sightImage;
    [SerializeField] Sprite[] sightSprites;

    // Update is called once per frame
    void Update()
    {
        if (!policeManager.policeCalled)
        {
            if (player.studentsSeePlayer == 0)
            {
                sightImage.sprite = sightSprites[0];
            }
            else
            {
                sightImage.sprite = sightSprites[1];
            }
        }
        else
        {
            sightImage.sprite = sightSprites[2];
        }
    }
}
