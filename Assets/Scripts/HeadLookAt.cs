﻿using UnityEngine;

public class HeadLookAt : MonoBehaviour
{
    [Header("Settings")]
    public Transform lookAtPoint;
    [SerializeField]
    private float rotationSpeed = 5;
    [SerializeField]
    private Vector2 maxHeadAngles;
    [Header("Skeleton")]
    [SerializeField]
    private Transform head;

    Quaternion rotationOffset = Quaternion.Euler(Vector3.zero);

    // Needs to happen in LateUpdate to override animation succesfully
    void LateUpdate()
    {
        if (lookAtPoint != null)
        {
            Vector3 lookDir = lookAtPoint.position - head.position;
            Quaternion lookRotation = Quaternion.LookRotation(lookDir);

            rotationOffset = Quaternion.Lerp(rotationOffset, Quaternion.Euler(lookRotation.eulerAngles - head.eulerAngles), rotationSpeed * Time.deltaTime);
        }
        else
        {
            rotationOffset = Quaternion.Lerp(rotationOffset, Quaternion.Euler(Vector3.zero), rotationSpeed * Time.deltaTime);
        }

        head.Rotate(rotationOffset.eulerAngles);

        // Limit rotations
        limitRotation(head, maxHeadAngles);
    }

    void limitRotation(Transform obj, Vector2 maxVector)
    {
        float yRot = obj.localEulerAngles.y;
        yRot = yRot > maxVector.y && yRot < 360 - maxVector.y && yRot < 180 ? maxVector.y : yRot;
        yRot = yRot < 360 - maxVector.y && yRot > maxVector.y && yRot > 180 ? 360 - maxVector.y : yRot;
        float xRot = obj.localEulerAngles.x;
        xRot = xRot > maxVector.x && xRot < 360 - maxVector.x && xRot < 180 ? maxVector.x : xRot;
        xRot = xRot < 360 - maxVector.x && xRot > maxVector.x && xRot > 180 ? 360 - maxVector.x : xRot;
        obj.localEulerAngles = new Vector3(xRot, yRot, obj.localEulerAngles.z);
    }
}
