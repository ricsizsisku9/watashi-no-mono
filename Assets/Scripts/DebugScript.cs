﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.UI;
using UnityEngine.AI;
//using Bearroll;

public class DebugScript : MonoBehaviour {
    public PlayerController player;
    public GameObject debugConsole;
    public MusicManager musicManager;
    public InputField debugText;
    public Text objectText;
    public Text logText;
    public GameObject malePrefab;
    public GameObject femalePrefab;
    public Material maleLashes;
    public Material femaleLashes;
    public Avatar AoiAvatar;
    public GameObject UI;
    float deltaTime = 0.0f;

    void Start()
    {
        string path = Application.streamingAssetsPath + "/Mods/modToRunOnStart.txt";

        if (File.Exists(path))
        {
            string[] lines = System.IO.File.ReadAllLines(path);
            runCommand("runmodscript " + lines[0]);
        }
    }

    playerMovementMode currentMovementMode;
    GameObject currentObjectSelected;
    void Update () {
        if (Input.GetButtonDown("Mute Music"))
        {
            if (MusicManager.musicPlaying())
            {
                MusicManager.stopMusic();
            }
            else
            {
                if (musicManager.saneMusic.clip.name.Contains("School_"))
                {
                    musicManager.randomizeMusic();
                }
                MusicManager.playMusic();
                SubtitlesUI.Get().AddText("[Music] : " + musicManager.saneMusic.clip.name, 5);
            }
        }

        #region Debug Hotkeys
        if (PlayerPrefs.GetInt("debugHotkeysEnabled") == 1)
        {
#if UNITY_EDITOR
            if (Input.GetKey(KeyCode.Joystick1Button7) || Input.GetKey(KeyCode.Escape))
            {
                UnityEditor.EditorApplication.isPlaying = false;
            }
#endif
            if (Input.GetKeyDown(KeyCode.Equals))
            {
                player.references.timeManager.wantedTimeScale += 2f;
            }
            if (Input.GetKeyDown(KeyCode.Minus))
            {
                if (player.references.timeManager.wantedTimeScale - 2f >= 1)
                {
                    player.references.timeManager.wantedTimeScale -= 2f;
                }
            }
            if (Input.GetKeyDown(KeyCode.BackQuote))
            {
                // If the player is holding shift while restarting the day, the save file will clear
                if (Input.GetKey(KeyCode.LeftShift))
                {
                    string path = Application.streamingAssetsPath + "/Saves/WNM_Save" + PlayerPrefs.GetInt("SaveFileID") + ".wnmsave";
                    FileStream file;

                    if (File.Exists(path))
                    {
                        File.Delete(path);
                        file = File.Create(path);
                        file.Close();
                    }
                }
                else
                {
                    SaveManager.saveGame(PlayerPrefs.GetInt("SaveFileID"));
                }
                SceneController.loadScene(SceneController.getSceneName());
            }
            if (player.transform.position.y < -15f)
            {
                //player.transform.position = new Vector3(0, 0, 0);
            }
            deltaTime += (Time.unscaledDeltaTime - deltaTime) * 0.1f;
        }
        #endregion
        #region Debug Console
        if (PlayerPrefs.GetInt("debugConsoleEnabled") == 1)
        {
            if (Input.GetButtonDown("Toggle Developer Menu"))
            {
                if (!debugConsole.activeInHierarchy)
                {
                    currentMovementMode = player.stats.canMove;
                    player.stats.canMove = playerMovementMode.cameraOnly;
                    player.references.mainCamera.rightOffset = 0.5f;
                    debugConsole.SetActive(true);
                    debugText.ActivateInputField();
                }
                else
                {
                    player.stats.canMove = currentMovementMode;
                    player.references.mainCamera.rightOffset = 0.15f;
                    debugConsole.SetActive(false);
                }
            }
            if (debugConsole.activeInHierarchy)
            {
                debugText.ActivateInputField();
                if (currentObjectSelected == null)
                {
                    objectText.text = "No Object Selected";
                }
                else
                {
                    objectText.text = "\"" + currentObjectSelected.name + "\"" + " (" + currentObjectSelected.GetInstanceID() + ")";
                }
                if (Input.GetKeyDown(KeyCode.KeypadEnter) || Input.GetKeyDown(KeyCode.Return))
                {
                    if (debugText.text != "")
                    {
                        runCommand(debugText.text);
                        debugText.text = "";
                    }
                }
                if (Input.GetMouseButtonDown(0))
                {
                    Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                    RaycastHit hit;

                    if (Physics.Raycast(ray, out hit))
                    {
                        currentObjectSelected = hit.transform.gameObject;
                    }
                    else
                    {
                        currentObjectSelected = null;
                    }
                }
            }
        }
        #endregion
    }

    string[] modcommands = new string[] {
        "wait ",
    };

    string[] commands = new string[] {
        "ffc",
        "kill",
        "revive",
        "setscale ",
        "genderswap",
        "find ",
        "findchild ",
        "runmodscript ",
        "movetome",
        "duplicate",
        "setasplayer",
        "setactive",
        "setinactive",
        "selectbundle ",
        "loadbundle ",
        "give ",
        "resetrotation",
        "forcepolice",
        "follow ",
        "toggleui"
    };

    AssetBundle assetBundle = null;
    public void runCommand(string commandText)
    {
        string commandFound = null;
        #region Find an existing command
        foreach (string command in commands)
        {
            if (commandText.ToLower().Contains(command))
            {
                commandFound = command;
                break;
            }
        }
        #endregion

        switch (commandFound)
        {
            case "ffc":
                #region free flying camera
                currentMovementMode = currentMovementMode == playerMovementMode.all ? playerMovementMode.cameraOnly : playerMovementMode.all;
                player.references.mainCamera.Type = player.references.mainCamera.Type == cameraType.Default ? cameraType.FreeFlying : cameraType.Default;
                throwLog("Free flying camera is now " + (player.references.mainCamera.Type == cameraType.Default ? "disabled" : "enabled"));
                break;
            #endregion
            case "kill":
                #region kill student
                if (currentObjectSelected == null)
                {
                    throwLog("Error: No object/student selected");
                }
                else
                {
                    Student student = currentObjectSelected.GetComponent<Student>();
                    if (student)
                    {
                        throwLog("Killed student " + currentObjectSelected.name);
                        student.character.pauseMovement();
                        student.character.status = characterStatus.Dead;
                        student.character.enableRagdoll();
                    }
                    else
                    {
                        throwLog("Error: 'Student' script not found");
                    }
                }
                break;
            #endregion
            case "revive":
                #region revive student
                if (currentObjectSelected == null)
                {
                    throwLog("Error: No object/student selected");
                }
                else
                {
                    Student student = currentObjectSelected.GetComponent<Student>();
                    if (student)
                    {
                        throwLog("Revived student " + currentObjectSelected.name);
                        student.character.resumeMovement();
                        student.character.status = characterStatus.Normal;
                        student.character.disableRagdoll();
                    }
                    else
                    {
                        throwLog("Error: 'Student' script not found");
                    }
                }
                break;
            #endregion
            case "setscale ":
                #region resize object
                if (currentObjectSelected == null)
                {
                    throwLog("Error: No object/student selected");
                }
                else
                {
                    float resizedScale;
                    if (float.TryParse(commandText.Replace(commandFound, ""), out resizedScale))
                    {
                        throwLog("Scaled " + currentObjectSelected.name + " to " + resizedScale);
                        currentObjectSelected.transform.localScale = new Vector3(resizedScale, resizedScale, resizedScale);
                    }
                    else
                    {
                        throwLog("Error: Unexpected characters");
                    }
                }
                break;
            #endregion
            case "genderswap":
                #region set student/character to the opposite gender
                if (currentObjectSelected == null)
                {
                    throwLog("Error: No object/student selected");
                }
                else
                {
                    if (currentObjectSelected.transform.Find("WNM_Female"))
                    {
                        #region set female to male
                        try
                        {
                            SkinnedMeshRenderer femClothing = currentObjectSelected.transform.Find("ClothingMesh").GetComponent<SkinnedMeshRenderer>();
                            SkinnedMeshRenderer malClothing = malePrefab.transform.Find("ClothingMesh").GetComponent<SkinnedMeshRenderer>();
                            SkinnedMeshRenderer femFace = currentObjectSelected.transform.Find("WNM_Female/PelvisRoot/Hips/Spine_0/Spine_1/Spine_2/Neck/Head/FaceMesh").GetComponent<SkinnedMeshRenderer>();
                            SkinnedMeshRenderer malFace = malePrefab.transform.Find("WNM_Male/PelvisRoot/Hips/Spine_0/Spine_1/Spine_2/Neck/Head/FaceMesh").GetComponent<SkinnedMeshRenderer>();
                            Animator femAnimator = currentObjectSelected.GetComponent<Animator>();
                            Animator malAnimator = malePrefab.GetComponent<Animator>();

                            femClothing.sharedMesh = malClothing.sharedMesh;
                            femClothing.materials = malClothing.sharedMaterials;
                            femFace.sharedMesh = malFace.sharedMesh;
                            Material[] newMats = femFace.materials;
                            newMats[1] = maleLashes;
                            femFace.materials = newMats;
                            femAnimator.enabled = false;
                            femFace.transform.parent.Find("Jaw").localEulerAngles = new Vector3(111.32f, 2.297989f, 2.456985f);
                            currentObjectSelected.transform.Find("WNM_Female").name = "WNM_Male";
                            femAnimator.avatar = malAnimator.avatar;

                            PlayerController p = currentObjectSelected.GetComponent<PlayerController>();
                            if (p)
                            {
                                p.canChangeClothing = false;
                                p.references.skirtMesh.enabled = false;
                                femAnimator.Play(p.stats.currentAnimation);
                            }

                            Student student = currentObjectSelected.GetComponent<Student>();
                            if (student)
                            {
                                student.character.characterGender = Gender.Male;
                                femAnimator.Play(student.character.currentAnimation);
                            }

                            femAnimator.enabled = true;

                            throwLog("Successfully converted " + currentObjectSelected.name + " to a male");
                        }
                        catch
                        {
                            throwLog("Error: Missing references");
                        }
                        #endregion
                    }
                    else
                    {
                        #region set male to female
                        try
                        {
                            SkinnedMeshRenderer malClothing = currentObjectSelected.transform.Find("ClothingMesh").GetComponent<SkinnedMeshRenderer>();
                            SkinnedMeshRenderer femClothing = femalePrefab.transform.Find("ClothingMesh").GetComponent<SkinnedMeshRenderer>();
                            SkinnedMeshRenderer malFace = currentObjectSelected.transform.Find("WNM_Male/PelvisRoot/Hips/Spine_0/Spine_1/Spine_2/Neck/Head/FaceMesh").GetComponent<SkinnedMeshRenderer>();
                            SkinnedMeshRenderer femFace = femalePrefab.transform.Find("WNM_Female/PelvisRoot/Hips/Spine_0/Spine_1/Spine_2/Neck/Head/FaceMesh").GetComponent<SkinnedMeshRenderer>();
                            Animator malAnimator = currentObjectSelected.GetComponent<Animator>();
                            Animator femAnimator = femalePrefab.GetComponent<Animator>();

                            malClothing.sharedMesh = femClothing.sharedMesh;
                            malClothing.materials = femClothing.sharedMaterials;
                            malFace.sharedMesh = femFace.sharedMesh;
                            Material[] newMats = malFace.materials;
                            newMats[1] = femaleLashes;
                            malFace.materials = newMats;
                            malAnimator.enabled = false;
                            malFace.transform.parent.Find("Jaw").localEulerAngles = new Vector3(115.618f, 0.003997803f, 0.003997803f);
                            currentObjectSelected.transform.Find("WNM_Male").name = "WNM_Female";
                            malAnimator.avatar = femAnimator.avatar;

                            PlayerController p = currentObjectSelected.GetComponent<PlayerController>();
                            if (p)
                            {
                                p.canChangeClothing = true;
                                p.references.skirtMesh.enabled = true;
                                malAnimator.Play(p.stats.currentAnimation);
                            }

                            Student student = currentObjectSelected.GetComponent<Student>();
                            if (student)
                            {
                                student.character.characterGender = Gender.Female;
                                malAnimator.Play(student.character.currentAnimation);
                            }

                            malAnimator.enabled = true;

                            throwLog("Successfully converted " + currentObjectSelected.name + " to a female");
                        }
                        catch
                        {
                            throwLog("Error: Missing references");
                        }
                        #endregion
                    }
                }
                break;
            #endregion
            case "find ":
                #region select an object by name or id
                GameObject result = null;
                // Get the wanted object's ID or name
                string objectName = commandText.Replace(commandFound, "");
                int objectID = 00000000;

                // Try to find the object via name & InstanceID
                foreach (GameObject go in Resources.FindObjectsOfTypeAll(typeof(GameObject)))
                {
                    if (go.name.ToLower() == objectName.ToLower())
                    {
                        result = go;
                        break;
                    }
                    else
                    {
                        // See if the object name is an InstanceID instead
                        if (int.TryParse(objectName, out objectID))
                        {
                            if (go.GetInstanceID() == objectID)
                            {
                                result = go;
                                break;
                            }
                        }
                    }
                }

                if (result != null)
                {
                    currentObjectSelected = result;
                    throwLog("Successfully found object: " + result.name + " (" + result.GetInstanceID() + ")");
                }
                else
                {
                    throwLog("Error: Could not find object specified");
                }
                break;
            #endregion
            case "findchild ":
                #region find an object inside the selected object
                string transformFind = commandText.Replace(commandFound, "");
                Debug.Log(transformFind);
                if (currentObjectSelected == null)
                {
                    throwLog("Error: No object/student selected");
                }
                else
                {
                    GameObject result1 = null;

                    foreach (Transform go in currentObjectSelected.GetComponentsInChildren<Transform>())
                    {
                        if (go.gameObject.name.ToLower() == transformFind.ToLower())
                        {
                            result1 = go.gameObject;
                        }
                    }

                    if (result1 == null)
                    {
                        throwLog("Error: Could not find object specified");
                    }
                    else
                    {
                        currentObjectSelected = result1.gameObject;
                        throwLog("Successfully found object: " + result1.name + " (" + result1.GetInstanceID() + ")");
                    }
                }
                break;
            #endregion
            case "runmodscript ":
                #region run a specified script in the mods folder
                throwLog("Running mod script");
                string modFilePath = commandText.Replace(commandFound, "");
                string path = Application.streamingAssetsPath + "/Mods/" + modFilePath;

                if (File.Exists(path))
                {
                    string[] lines = System.IO.File.ReadAllLines(path);
                    StartCoroutine(runModLines(lines));
                }
                break;
            #endregion
            case "movetome":
                #region move the object to uchikina
                if (currentObjectSelected == null)
                {
                    throwLog("Error: No object/student selected");
                }
                else
                {
                    currentObjectSelected.transform.position = player.transform.position;
                    throwLog("Moved " + currentObjectSelected.name + " to " + player.transform.position);
                }
                break;
            #endregion
            case "duplicate":
                #region duplicate the object
                if (currentObjectSelected == null)
                {
                    throwLog("Error: No object/student selected");
                }
                else
                {
                    throwLog("Successfully duplicated " + currentObjectSelected.name);
                    currentObjectSelected = Instantiate(currentObjectSelected);
                    currentObjectSelected.transform.position = player.transform.position;
                }
                break;
            #endregion
            case "setasplayer":
                #region set an object to the player
                if (currentObjectSelected == null)
                {
                    throwLog("Error: No object/student selected");
                }
                else
                {
                    if (!currentObjectSelected.isStatic)
                    {
                        foreach (SkinnedMeshRenderer mesh in player.GetComponentsInChildren<SkinnedMeshRenderer>())
                        {
                            mesh.gameObject.SetActive(false);
                        }

                        foreach (MeshRenderer mesh in player.GetComponentsInChildren<MeshRenderer>())
                        {
                            mesh.gameObject.SetActive(false);
                        }

                        //player.transform.Find("WNM_Female").name = "RENAMEDBoneset";
                        //player.transform.Find("RENAMEDBoneset").gameObject.SetActive(false);
                        currentObjectSelected = Instantiate(currentObjectSelected);

                        foreach (Collider col in currentObjectSelected.GetComponentsInChildren<Collider>())
                        {
                            if (col.name == "HairCollider")
                            {
                                col.gameObject.SetActive(false);
                            }
                        }

                        Rigidbody component0 = currentObjectSelected.GetComponent<Rigidbody>();
                        if (component0)
                        {
                            component0.isKinematic = true;
                        }

                        Student component1 = currentObjectSelected.GetComponent<Student>();
                        if (component1)
                        {
                            //component1.character.lookAt(player.references.mainCamera.lookTarget);
                            //component1.HeadLook.lookAtPoint = player.references.mainCamera.lookTarget;
                            component1.enabled = false;
                        }

                        Collider component2 = currentObjectSelected.GetComponent<Collider>();
                        if (component2)
                        {
                            component2.enabled = false;
                        }

                        NavMeshAgent component3 = currentObjectSelected.GetComponent<NavMeshAgent>();
                        if (component3)
                        {
                            Destroy(component3);
                        }

                        Animation component35 = currentObjectSelected.GetComponent<Animation>();
                        if (component35)
                        {
                            Destroy(component35);
                            GameObject currentObject = currentObjectSelected;
                            runCommand("findchild pelvisroot");
                            Animator newAnimator = currentObjectSelected.transform.parent.gameObject.AddComponent<Animator>();
                            newAnimator.avatar = AoiAvatar;

                            newAnimator.runtimeAnimatorController = player.references.animator.runtimeAnimatorController;
                            player.references.animator = newAnimator;

                            currentObjectSelected = currentObject;
                        }

                        Animator component4 = currentObjectSelected.GetComponent<Animator>();
                        if (component4)
                        {
                            component4.runtimeAnimatorController = player.references.animator.runtimeAnimatorController;
                            player.references.animator = component4;
                        }

                        /*GDOC_Occludee component5 = currentObjectSelected.GetComponent<GDOC_Occludee>();
                        if (component5)
                        {
                            Destroy(component5);
                        }*/

                        currentObjectSelected.transform.SetParent(player.transform);
                        currentObjectSelected.transform.localEulerAngles = Vector3.zero;
                        currentObjectSelected.transform.localPosition = Vector3.zero;

                        throwLog("Successfully set as player: " + currentObjectSelected.name);
                    }
                    else
                    {
                        throwLog("Cannot set a static object as the player");
                    }
                }
                break;
            #endregion
            case "setactive":
                #region set the selected gameobject as active
                if (currentObjectSelected == null)
                {
                    throwLog("Error: No object/student selected");
                }
                else
                {
                    currentObjectSelected.SetActive(true);
                    throwLog("Set GameObject " + currentObjectSelected.name + " to active");
                }
                break;
            #endregion
            case "setinactive":
                #region set the selected gameobject as inactive
                if (currentObjectSelected == null)
                {
                    throwLog("Error: No object/student selected");
                }
                else
                {
                    currentObjectSelected.SetActive(false);
                    throwLog("Set GameObject " + currentObjectSelected.name + " to inactive");
                }
                break;
            #endregion
            case "selectbundle ":
                #region select a bundle to import
                string bundlepath = commandText.Replace(commandFound, "");
                assetBundle = AssetBundle.LoadFromFile(Application.streamingAssetsPath + "/Mods/" + bundlepath);
                if (assetBundle != null)
                {
                    throwLog("Successfully selected assetbundle: " + bundlepath);
                }
                else
                {
                    throwLog("Error: Could not read or find bundle: " + bundlepath);
                }
                break;
            #endregion
            case "loadbundle ":
                #region import an asset from the selected bundle
                if (assetBundle != null)
                {
                    string bundleassetname = commandText.Replace(commandFound, "");

                    var prefab = assetBundle.LoadAsset<GameObject>(bundleassetname);
                    assetBundle.Unload(false);

                    if (prefab != null)
                    {
                        throwLog("Successfully added into scene: " + bundleassetname);
                        currentObjectSelected = Instantiate(prefab);
                        currentObjectSelected.transform.position = player.transform.position;
                    }
                    else
                    {
                        throwLog("Error: Could not find or import bundle asset: " + bundleassetname);
                    }
                }
                else
                {
                    throwLog("Error: No assetbundle is selected");
                }
                break;
            #endregion
            case "give ":
                #region give the player an item
                string itemName = commandText.Replace(commandFound, "");
                runCommand("find " + itemName);

                Item tool = currentObjectSelected.GetComponent<Item>();
                if (tool)
                {
                    Instantiate(currentObjectSelected).GetComponent<Item>();
                    currentObjectSelected = tool.gameObject;
                    throwLog("Giving Uchikina: " + currentObjectSelected.name);
                    tool.myPrompt.info[0].OnFill.Invoke();
                }
                else
                {
                    throwLog("Error: 'Item' script not found");
                }
                break;
            #endregion
            case "resetrotation":
                #region reset the current object's rotation
                if (currentObjectSelected == null)
                {
                    throwLog("Error: No object/student selected");
                }
                else
                {
                    currentObjectSelected.transform.eulerAngles = new Vector3(-90, 0, 0);
                    throwLog("Rotation resetted: " + currentObjectSelected.name);
                }
                break;
            #endregion
            case "forcepolice":
                #region make the police arrive
                PoliceManager policeManager = GameObject.FindObjectOfType<PoliceManager>();
                policeManager.StartCoroutine(policeManager.interrogatePlayer());
                debugConsole.SetActive(false);
                break;
            #endregion
            case "follow ":
                #region make student follow/go to an object
                if (currentObjectSelected == null)
                {
                    throwLog("Error: No object/student selected");
                }
                else
                {
                    Student student = currentObjectSelected.GetComponent<Student>();
                    if (student)
                    {
                        string objname = commandText.Replace(commandFound, "");
                        currentObjectSelected = null;
                        runCommand("find " + objname);
                        if (currentObjectSelected)
                        {
                            throwLog(student.name + " is now following " + currentObjectSelected.name);
                            student.character.follow(currentObjectSelected.transform, followType.RunAndWalk);
                        }
                    }
                    else
                    {
                        throwLog("Error: 'Student' script not found");
                    }
                }
                break;
            #endregion
            case "toggleui":
                #region toggle ui
                UI.SetActive(!UI.activeInHierarchy);
                #endregion
                break;
        }

        if (commandFound == null) throwLog("Invalid command");
    }

    void throwLog(string text)
    {
        logText.text = text + "\n" + logText.text;
    }

    public IEnumerator runModLines(string[] lines)
    {
        foreach (string line in lines)
        {
            string commandFound = null;

            #region Find an existing command
            foreach (string command in modcommands)
            {
                if (line.ToLower().Contains(command))
                {
                    commandFound = command;
                    break;
                }
            }
            #endregion

            switch (commandFound)
            {
                case "wait ":
                    #region wait a specified time before going to the next line
                    float timeToWait;
                    if (float.TryParse(line.Replace(commandFound, ""), out timeToWait))
                    {
                        throwLog("Waiting " + timeToWait + " seconds before resuming");
                        yield return new WaitForSeconds(timeToWait);
                    }
                    else
                    {
                        throwLog("Error: Unexpected characters");
                    }
                    break;
                #endregion
            }

            if (commandFound == null) runCommand(line);
        }
    }

    void OnGUI()
    {
        int w = Screen.width, h = Screen.height;

        GUIStyle style = new GUIStyle();

        Rect rect = new Rect(0, 0, w, h * 2 / 100);
        style.alignment = TextAnchor.UpperLeft;
        style.fontSize = h * 2 / 100;
        style.normal.textColor = new Color(0.0f, 0.0f, 0.5f, 1.0f);
        float msec = deltaTime * 1000.0f;
        float fps = 1.0f / deltaTime;
        string text = string.Format("{0:0.0} ms ({1:0.} fps)", msec, fps);
        GUI.Label(rect, text, style);
    }
}
