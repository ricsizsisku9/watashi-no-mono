﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerLocker : MonoBehaviour {
    [SerializeField] Transform lockerDoor;
    [SerializeField] Prompt lockerPrompt;
    [SerializeField] PlayerController player;
    [SerializeField] ParticleSystem particles;
    [SerializeField] bool open;

    public void lookInLocker()
    {
        open = true;
        lockerPrompt.active = false;
    }
    
    public void leaveLocker()
    {
        open = false;
        lockerPrompt.active = true;
    }

	void Update ()
    {
        if (open)
        {
            particles.enableEmission = false;
            lockerDoor.transform.localRotation = Quaternion.Lerp(lockerDoor.transform.localRotation, Quaternion.Euler(new Vector3(0, -235, 0)), Time.deltaTime * 8);
        }
        else
        {
            particles.enableEmission = true;
            lockerDoor.transform.localRotation = Quaternion.Lerp(lockerDoor.transform.localRotation, Quaternion.Euler(new Vector3(0, -90, 0)), Time.deltaTime * 8);
        }
	}
}
