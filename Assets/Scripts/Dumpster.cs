﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dumpster : MonoBehaviour {
    bool dumpsterMoved;
    public bool rotateEnabled;
    public Vector3[] positions;
    public Vector3[] eulerAngles;
    public DumpsterLid[] doors;

	void Update () {
		if (!dumpsterMoved)
        {
            transform.position = Vector3.Lerp(transform.position, positions[0], Time.deltaTime * 3);
            if (rotateEnabled)
            {
                transform.localEulerAngles = Vector3.Lerp(transform.localEulerAngles, eulerAngles[0], Time.deltaTime * 3);
            }
        }
        else
        {
            transform.position = Vector3.Lerp(transform.position, positions[1], Time.deltaTime * 3);
            if (rotateEnabled)
            {
                transform.localEulerAngles = Vector3.Lerp(transform.localEulerAngles, eulerAngles[1], Time.deltaTime * 3);
            }
        }
	}

    public void toggleDumpsterPosition()
    {
        dumpsterMoved = !dumpsterMoved;
        SoundManager.Play("SlidingDoorSound");
        foreach (DumpsterLid door in doors)
        {
            if (door.open)
            {
                door.open = false;
                SoundManager.Play("IncineratorClose");
            }
        }
    }
}
