﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Student : MonoBehaviour
{
    #region Variables
    [Header("== Student Information ==")]
    public Persona studentPersona;

    [Header("Animations")]
    public movementAnimations movementAnimation;
    public string currentAnimation = "";

    [Header("Stats")]
    public float hunger = 100;
    public float thirst = 100;

    [Header("Social")]
    public Student[] friends;
    public Student[] enemies;
    public Student crush;
    public int complimented;
    public int timesTalkedTo;
    public float playerOpinion = 1f;
    public bool busy;
    public bool metPlayer;

    #region Responses to a talking player
    public string[] greetMessages = new string[]
    {
        "Hm?",
        "Yeah?",
        "Huh?"
    };
    public string[] rudeGreetMessages = new string[]
    {
        "What.",
        "What do you want...",
        "Oh. It's you."
    };
    public string[] meetMessages = new string[]
    {
        "Oh! I'm %studentname%. Nice to meet you!",
        "I'm %studentname%. Good to meet you.",
    };
    public string[] complimentResponses = new string[]
    {
        "Really? Wow! Thank you so much!",
        "R-Really? Thanks...!",
        "Are you sure...? I kind of rushed out of bed this morning... But thanks!",
        "You think so? Thanks!"
    };
    #endregion

    [Header("Destinations")]
    public Destination freetimeSpot;
    public Destination lunchSpot;
    public Destination classDesk;
    public Destination homeSpot;
    public bool inScriptedEvent;

    [Header("References")]
    public Character character;

    // Managers
    public ClockUI timeManager;
    public PlayerController player;
    public CharacterMouth mouthMovementScript;
    public Students studentManager;
    public HeadLookAt HeadLook;

    // Misc. References
    public ParticleSystem[] damageEffects;
    public Prompt myPrompt;
    public Transform Head;
    public Transform Chest;
    public Rigidbody dragLimb;
    public Rigidbody[] dragArms;
    bool witnessingSuspiciousPlayer;
    bool timeEventChanged;

    [SerializeField] ParticleSystem[] emotes;

    public Student studentVictimWitnessed;
    public int currentSelectedStudent;
    bool sightedPlayer;

    #endregion

    // Set up pathfinding
    void Start()
    {
        if (freetimeSpot) character.setDestination(freetimeSpot, false);
    }

    int currentFrame = 0;
    int frameInterval = 5;
    void Update()
    {
        // This portion of code will run every frame
        updateMovement();
        updatePrompt();

        // This will run every five frames to save performance
        if (currentFrame == frameInterval)
        {
            currentFrame = 0;
            updateVision();
            updateTalking();
            //updateLOD();
        }
        else
        {
            currentFrame++;
        }
    }

    // Change the prompt depending on the player's variables
    void updatePrompt()
    {
        myPrompt.transform.position = Head.position + new Vector3(0, 0.25f, 0);
        if (!character.ragdoll.ragdolled)
        {
            if (!player.stats.holdingWeapon())
            {
                if (character.status == characterStatus.Normal || character.status == characterStatus.Suspicious)
                {
                    myPrompt.active = true;
                    myPrompt.info[0].promptText = "Talk";
                    myPrompt.info[0].promptKey = "Interact";
                    myPrompt.info[0].renderType = promptType.Normal;
                }
                else
                {
                    myPrompt.active = false;
                }
            }
            else
            {
                if (character.status != characterStatus.BeingShoved)
                {
                    myPrompt.active = true;
                    myPrompt.info[0].promptText = "Attack";
                    myPrompt.info[0].promptKey = "Attack";
                    myPrompt.info[0].renderType = promptType.Dangerous;
                }
                else
                {
                    myPrompt.active = false;
                }
            }
        }
        else
        {
            // Set the prompt's text if the player is near the incinerator or if the player is dragging the dead student or not
            if (player.stats.playerState != playerStatus.Dragging)
            {
                myPrompt.active = true;
                myPrompt.info[0].promptText = "Drag";
                myPrompt.info[0].promptKey = "Pick up";
                myPrompt.info[0].renderType = promptType.Dangerous;
            }
            else
            {
                myPrompt.active = true;
                if (Vector3.Magnitude(player.transform.position - player.references.incinerator.myPrompt.transform.position) < 2f)
                {
                    if (player.references.incinerator.open)
                    {
                        if (player.stats.strength >= 2)
                        {
                            myPrompt.info[0].promptText = "Dispose";
                            myPrompt.info[0].renderType = promptType.Dangerous;
                        }
                        else
                        {
                            myPrompt.info[0].promptText = "Too Weak!";
                            myPrompt.info[0].renderType = promptType.Dangerous;
                        }
                    }
                    else
                    {
                        myPrompt.info[0].promptText = "Door is closed!";
                        myPrompt.info[0].promptKey = "Pick up";
                        myPrompt.info[0].renderType = promptType.Normal;
                    }
                }
                else
                {
                    myPrompt.info[0].promptText = "Drop";
                    myPrompt.info[0].promptKey = "Pick up";
                    myPrompt.info[0].renderType = promptType.Dangerous;
                }
            }
        }
    }

    // Open doors
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Door")
        {
            if (!other.GetComponent<Door>().open)
            {
                other.GetComponent<Door>().open = true;
                other.GetComponent<AudioSource>().PlayOneShot(other.GetComponent<AudioSource>().clip);
            }
        }
    }

    // Play particle effects to emote
    public void emote(studentEmotes emoteType)
    {
        switch (emoteType)
        {
            case studentEmotes.Scare:
                emotes[0].Play();
                break;
            case studentEmotes.OpinionPlus:
                emotes[1].Play();
                break;
            case studentEmotes.OpinionMinus:
                emotes[2].Play();
                break;
            case studentEmotes.Suspicion:
                emotes[3].transform.LookAt(player.references.mainCamera.transform.position);
                emotes[3].Play();
                break;
        }
    }


    // Check if the prompt is filled and then be interacted with
    public void Interact()
    {
        if (character.status != characterStatus.Dead && character.status != characterStatus.Unconsious)
        {
            if (!player.stats.holdingWeapon())
            {
                // Talk to the student
                if (!busy && timeManager.currentEvent.eventName != "Classtime" && timeManager.currentEvent.eventName != "Class Preparation" && character.status == characterStatus.Normal)
                {
                    startTalking();
                }
                else
                {
                    randomMessage(new string[] {
                        "Sorry, I'm busy right now.",
                        "Can't talk right now.",
                        "Now's not the time.",
                        "We can talk later.",
                        "Sorry, not right now.",
                        "I'm busy.",
                        "Not now.",
                        "Let's talk later.",
                        "Not right now."
                    });
                }
            }
            else
            {
                // Use the equipped item on the student
                player.stats.selectedStudent = this;
                player.stats.promptsEnabled = false;
                player.stats.canMove = playerMovementMode.none;
                character.lookAt(player.references.Head);
                character.pauseMovement();
                player.StartCoroutine(player.references.attackManager.attackEnum());
            }
        }
        else
        {
            // Drag the student
            player.drag(this);
        }
    }

    void startTalking()
    {
        if (character.currentAnimation != character.movementAnimation.sitAnim)
        {
            character.playAnimation(character.movementAnimation.idleAnim, 0.2f);
            character.lookAt(player.references.Head);
        }
        if (player.stats.currentAnimation != player.moveAnims.sitAnim)
        {
            player.playAnimation("Wave", 0.25f);
        }
        if (!player.references.talkManager.active)
        {
            player.references.talkManager.selectedStudent = this;
            player.references.talkManager.activate(null);
            player.references.talkManager.setChoices("Sorry, nevermind.", "You look great today!", "I'm Uchikina.", "Can I ask you something?", null);
            if (playerOpinion > 0.6f)
            {
                player.references.talkManager.randomMessage(greetMessages);
            }
            else
            {
                player.references.talkManager.randomMessage(rudeGreetMessages);
            }
        }

        player.references.headLook.lookAtPoint = Head;
        HeadLook.lookAtPoint = player.references.Head;

        player.stats.playerState = playerStatus.Talking;
        player.stats.selectedStudent = this;
        player.stats.promptsEnabled = false;
        player.stats.canMove = playerMovementMode.none;
        character.playFaceAnimation("FaceBlink", 0.25f);
        character.status = characterStatus.Talking;
        character.pauseMovement();
    }

    // Update the student's movement
    float lerpTime = 2f;
    void updateMovement()
    {
        // Detect if the student is on the destination
        if (character.currentDestination != null)
        {
            if (Vector3.Magnitude(character.currentDestination.transform.position - transform.position) < 0.02f && character.lookTarget == null)
            {
                character.arrivedAtDestination = true;
            }
            else
            {
                character.arrivedAtDestination = false;
            }
        }
        else
        {
            character.arrivedAtDestination = false;
        }

        // See what to do on arrival
        if (character.arrivedAtDestination)
        {
            if (player.stats.selectedStudent != this)
            {
                // Freeze the student
                if (character.movementAgent.enabled) character.pauseMovement();

                if (character.status != characterStatus.Scared || character.currentDestination.actionOnArrival == DestinationAction.Disappear)
                {
                    // Slide into those DMs
                    if (!character.ragdoll.ragdolled)
                    {
                        if (character.lookTarget == null)
                        {
                            transform.position = Vector3.Lerp(transform.position, character.currentDestination.gameObject.transform.position, Time.deltaTime * lerpTime);
                            Quaternion newRot = Quaternion.Euler(new Vector3(0, character.currentDestination.transform.eulerAngles.y, 0));
                            transform.rotation = Quaternion.Lerp(transform.rotation, newRot, Time.deltaTime * lerpTime * 2);
                        }
                        // Animation
                        if (!character.spotAnimated)
                        {
                            character.spotAnimated = true;
                            if (!inScriptedEvent)
                            {
                                initiateSpotAnimation(character.currentDestination);
                            }
                            else
                            {
                                character.playAnimation(character.movementAnimation.idleAnim, 0.25f);
                            }
                        }
                    }
                }
            }
        }
        
        // Go to the next important scheduel if it changed
        if (timeEventChanged)
        {
            timeEventChanged = false;
            if ((player.stats.playerState == playerStatus.Talking && player.stats.selectedStudent == this) || character.followTarget == player.transform)
            {
                player.stopTalking();
                SubtitlesUI.Get().AddText("I gotta go. Sorry.", 5f);
            }
            else if (player.stats.playerState == playerStatus.Eavesdropping && player.stats.currentStudentEavesDropping == this)
            {
                SubtitlesUI.Get().AddText("Oh... let's talk later.", 5f);
            }

            switch (timeManager.currentEvent.eventName)
            {
                case "Morning":
                    character.setDestination(freetimeSpot, false);
                    break;
                case "Class Preparation":
                    character.setDestination(classDesk, false);
                    break;
                case "Classtime":
                    if (character.currentDestination != classDesk) { character.setDestination(classDesk, true); }
                    break;
                case "Lunchtime":
                    if (lunchSpot == null) lunchSpot = freetimeSpot;
                    character.setDestination(lunchSpot, false);
                    break;
                case "Cleaning Time":
                    // Not done yet ;(
                    character.setDestination(freetimeSpot, false);
                    break;
                case "After School":
                    character.setDestination(homeSpot, false);
                    break;
                case "Lockdown":
                    character.setDestination(classDesk, true);
                    break;
            }
        }

        if (character.status == characterStatus.Dead && character.status == characterStatus.Unconsious)
        {
            character.pauseMovement();
        }

        transform.eulerAngles = new Vector3(0, transform.eulerAngles.y, 0);
    }

    public void onTimeEventChanged()
    {
        timeEventChanged = true;
    }

    // Do an action depending on the destination's chosen action
    bool canAnimateSpotAnimation;
    bool waitingForArrival;
    public void initiateSpotAnimation(Destination destination)
    {
        if (destination.requiredArrivals.Count == 0)
        {
            canAnimateSpotAnimation = true;
            waitingForArrival = false;
        }
        else
        {
            if (canAnimateSpotAnimation == false)
            {
                waitingForArrival = true;
            }
        }

        if (canAnimateSpotAnimation)
        {
            switch (destination.actionOnArrival)
            {
                case DestinationAction.Stand:
                    character.playAnimation(character.movementAnimation.idleAnim, 0.5f);
                    break;
                case DestinationAction.Sit:
                    character.playAnimation(character.movementAnimation.sitAnim, 0.5f);
                    break;
                case DestinationAction.LookAt:
                    character.playAnimation("LookAt", 0.5f);
                    break;
                case DestinationAction.TalkSolo:
                    character.playAnimation("TalkSolo", 0.5f);
                    if (!character.ragdoll.ragdolled) character.playFaceAnimation("Face_Talk", 0.2f);
                    break;
                case DestinationAction.Disappear:
                    if (character.status == characterStatus.Scared)
                        player.references.policeManager.policeCalled = true;
                    gameObject.SetActive(false);
                    break;
            }
        }
        else
        {
            character.playAnimation(character.movementAnimation.idleAnim, 0.5f);
        }
    }

    // Check if the student is being talked to
    void updateTalking()
    {
        if (character.status == characterStatus.Talking)
        {
            // Student is being talked to
            character.characterBody.isKinematic = true;
            character.lookAt(transform.name == "Setsumi Kukiko" ? player.references.mainCamera.transform : player.references.Head);
        }
        if (player.stats.playerState == playerStatus.Attacking && player.stats.selectedStudent == this)
        {
            character.characterBody.isKinematic = true;
            character.lookAt(player.transform);
        }
    }

    // Set the subtitles to a random message
    void randomMessage(string[] messages)
    {
        int random = Random.Range(0, messages.Length);
        SubtitlesUI.Get().AddText(messages[random], 5f);
    }

    // Scream and alert students nearby
    public void Scream()
    {
        // Play a random sound
        SoundManager.PlayRandom(new string[12] {
            "FemaleScream1",
            "FemaleScream2",
            "FemaleScream3",
            "FemaleScream4",
            "FemaleScream5",
            "FemaleScream6",
            "FemaleScream7",
            "FemaleScream8",
            "FemaleScream9",
            "FemaleScream10",
            "FemaleScream11",
            "FemaleScream12",
        }, character.audioSource, PlayerPrefs.GetFloat("voiceVolume"));
        if (Operators.Distance(player.references.mainCamera.transform.position, transform.position) < character.audioSource.maxDistance)
        {
            SubtitlesUI.Get().AddText("[ SCREAM ]", 5);
        }
        emote(studentEmotes.Scare);
        // Alert nearby students
        foreach (Student student in studentManager.allStudents)
        {
            if (student != this)
            {
                if (Vector3.Magnitude(student.transform.position - transform.position) < 25 && Mathf.Abs(student.transform.position.y - transform.position.y) < 0.1f)
                {
                    if (student.character.status == characterStatus.Normal)
                    {
                        StartCoroutine(student.noticeScream(this));
                    }
                }
            }
        }
    }

    // React to a dead body
    public IEnumerator reactToBody(Student deadBody)
    {
        switch (studentPersona)
        {
            case Persona.Default:
                Scream();
                character.playAnimation("Panic", 0.25f);
                randomMessage(new string[] {
                    "Oh my god...!",
                    "JESUS CHRIST!",
                    "Oh my god... are they dead...?!",
                    "OH MY GOD! I NEED TO GET OUT OF HERE!"
                });
                yield return new WaitForSeconds(3);
                character.resumeMovement();
                character.setDestination(homeSpot, true);
                character.lookAt(null);
                break;
        }
    }

    // React to a murder
    IEnumerator reactToMurder()
    {
        switch (studentPersona)
        {
            case Persona.Default:
                Scream();
                character.playAnimation("Panic", 0.25f);
                randomMessage(new string[] {
                    "OH MY GOD!",
                    "What are you- OH SHIT!",
                    "OH NO!",
                    "Oh... my god..."
                });
                yield return new WaitForSeconds(Random.Range(1.5f, 5f));
                character.lookAt(null);
                character.resumeMovement();
                character.setDestination(homeSpot, true);
                break;
        }
    }

    // Update the student's vision to see corpses and other alarming things
    void updateVision()
    {
        if (character.status != characterStatus.Dead && character.status != characterStatus.Unconsious)
        {
            // Check the player
            if (character.canSee(player.references.Head))
            {
                if (!sightedPlayer)
                {
                    sightedPlayer = true;
                    player.studentsSeePlayer += 1;
                }
                checkPlayer();
            }
            else
            {
                if (sightedPlayer)
                {
                    sightedPlayer = false;
                    player.studentsSeePlayer -= 1;
                }
            }

            // Check students
            currentSelectedStudent = currentSelectedStudent > studentManager.allStudents.Length - 1 ? 0 : currentSelectedStudent;
            Student student = studentManager.allStudents[currentSelectedStudent];

            if (student.character.status == characterStatus.Dead || student.character.status == characterStatus.Unconsious && student != this)
            {
                checkStudent(student);
            }
            if (waitingForArrival)
            {
                checkStudentArrived(student);
            }

            currentSelectedStudent += 1;
        }
        else
        {
            if (sightedPlayer)
            {
                sightedPlayer = false;
                player.studentsSeePlayer -= 1;
            }
        }
    }

    bool canSeeBody(Student student)
    {
            if (character.canSee(student.character.ragdoll.Limbs["Head"]))
            return true;
        else if (character.canSee(student.character.ragdoll.Limbs["Hips"]))
            return true;
        else if (character.canSee(student.character.ragdoll.Limbs["Spine_2"]))
            return true;
        else if (character.canSee(student.character.ragdoll.Limbs["Spine_1"]))
            return true;
        
        return false;
    }

    // Check if the student has arrived at their spot
    void checkStudentArrived(Student student)
    {
        if (character.currentDestination.requiredArrivals.Contains(student.character.currentDestination))
        {
            if (student.character.arrivedAtDestination && student.character.lookTarget == null)
            {
                waitingForArrival = false;
                canAnimateSpotAnimation = true;
                character.spotAnimated = false;
            }
            else
            {
                canAnimateSpotAnimation = false;
                character.spotAnimated = false;
            }
        }
    }

    // Check if a student is dead and if the player is next to it
    void checkStudent(Student student)
    {
        if (character.status != characterStatus.Scared)
        {
            studentVictimWitnessed = student;
            if (canSeeBody(student))
            {
                if (!sightedPlayer || (sightedPlayer && player.stats.playerState != playerStatus.Attacking))
                {
                    if (student.character.ragdoll.ragdolled && !character.ragdoll.ragdolled && character.status != characterStatus.Scared)
                    {
                        character.status = characterStatus.Scared;
                        character.playFaceAnimation("Face_Scared", 0.25f);
                        character.pauseMovement();
                        StartCoroutine(reactToBody(student));
                    }
                    if (character.status != characterStatus.Scared)
                        studentVictimWitnessed = null;
                }
                else
                {
                    if (canSeeBody(student) && character.canSee(player.references.Head))
                    {
                        checkPlayer();
                    }
                }
            }
            else
            {
                currentSelectedStudent++;
            }
        }
        else
        {
            if (character.currentDestination != homeSpot)
            {
                character.lookAt(player.references.Head);
            }
            else
            {
                character.lookAt(null);
            }
        }
    }

    // Check if the student should react to the player
    void checkPlayer()
    {
        playerReactionTypes reaction = playerReactionTypes.NoReaction;

        if (player.stats.playerState == playerStatus.Attacking)
        {
            reaction = playerReactionTypes.Murder;
        }
        else if (player.isSuspicious(this) != playerSuspicionTypes.normal)
        {
            reaction = playerReactionTypes.Suspicious;
        }

        if (player.stats.holdingWeapon())
        {
            // See if the player is looking away from the student
            float angle = Vector3.Angle(player.transform.forward, transform.position - player.transform.position);
            if (angle > 80)
            {
                reaction = playerReactionTypes.Suspicious;
            }
        }

        // See what kind of reaction the student should do and react
        if (!character.ragdoll.ragdolled && character.canSee(player.references.Head))
        {
            if (reaction == playerReactionTypes.Murder)
            {
                #region React to the player murdering someone
                if (character.status != characterStatus.Scared)
                {
                    if (!character.ragdoll.ragdolled)
                    {
                        character.status = characterStatus.Scared;
                        character.playFaceAnimation("Face_Scared", 0.25f);
                        character.pauseMovement();
                        player.references.policeManager.murderWitnesses++;
                        StartCoroutine(reactToMurder());
                    }
                }
                else
                {
                    if (character.currentDestination != homeSpot)
                    {
                        character.lookAt(player.references.Head);
                    }
                }
                #endregion
            }
            else if (reaction == playerReactionTypes.Suspicious)
            {
                #region Stop and look at the player for being suspicious
                if (!witnessingSuspiciousPlayer && !character.ragdoll.ragdolled && character.status != characterStatus.Scared)
                {
                    witnessingSuspiciousPlayer = true;
                    StartCoroutine(reactToSuspiciousPlayer(player.isSuspicious(this)));
                }
                #endregion
            }
        }
    }

    IEnumerator reactToSuspiciousPlayer(playerSuspicionTypes reason)
    {
        Debug.Log("Looking at suspicious player");
        yield return new WaitForSeconds(2f);
        if (character.status == characterStatus.Normal && character.canSee(player.references.Head) && player.isSuspicious(this) != playerSuspicionTypes.normal && !character.ragdoll.ragdolled)
        {
            Destination lastSpot = character.currentDestination;
            SoundManager.Play("NoticePlayer", PlayerPrefs.GetFloat("effectVolume"));
            playerOpinion -= 0.1f;
            character.status = characterStatus.Suspicious;
            character.pauseMovement();
            character.playAnimation("Investigate", 0.25f);
            string[] subtitleText = new string[1] { "If you see this message, please report it as a bug to EpicMeal-Dev." };
            switch (reason)
            {
                case playerSuspicionTypes.bloody:
                    #region Bloody player responses
                    subtitleText = new string[] {
                        "Is that blood...?",
                        "Why are you covered in blood?",
                        "What's with you...?",
                        "Is that paint or...?",
                        "Are you okay...?" };
                    break;
                #endregion
                case playerSuspicionTypes.insane:
                    #region Insane player responses
                    subtitleText = new string[] {
                        "You're acting really weird...",
                        "Please stop... you're creeping me out.",
                        "Um... Can I help you?",
                        "What are you doing?",
                        "Are you okay...?" };
                    break;
                #endregion
                case playerSuspicionTypes.hasSuspiciousItem:
                    #region Suspicious item responses
                    subtitleText = new string[] {
                        "Wh... Why do you have that?",
                        "What is that?",
                        "Why do you have that?",
                        "Hey, what is that?",
                        "Hey, keep that thing away!" };
                    break;
                    #endregion
            }
            SubtitlesUI.Get().AddText(subtitleText[Random.Range(0, subtitleText.Length)], 5f);
            character.lookAt(player.references.Head);
            yield return new WaitForSeconds(Random.Range(4, 6));
            // Return to the original destination if the student didn't die or become scared
            if (character.status == characterStatus.Suspicious)
            {
                character.lookAt(null);
                character.resumeMovement();
                character.status = characterStatus.Normal;
                character.setDestination(lastSpot, false);
                playerOpinion -= 0.1f;
            }
        }
        witnessingSuspiciousPlayer = false;
    }

    // Get shoved by a character
    public int timesShoved;
    public IEnumerator shove(Transform origin)
    {
        characterStatus originalStatus = character.status;
        character.status = characterStatus.BeingShoved;
        character.pauseMovement();
        character.movementAgent.enabled = true;
        emote(studentEmotes.OpinionMinus);

        if (inScriptedEvent)
        {
            GameObject.FindObjectOfType<ScriptedEvents>().cancelEvent = true;
        }

        if (origin == player.transform)
        {
            timesShoved++;
        }

        GameObject empty = new GameObject();
        empty.transform.position = origin.position;

        character.lookAt(empty.transform);

        // Angry brows
        character.faceMesh.SetBlendShapeWeight(2, 100);

        string[] randomMessages0 = new string[] 
        {
            "Ah!",
            "Wh-",
            "Hey!",
            "What the-",
            "What the hell!",
            "HEY!",
            "Ah-",
            "OW!",
            "Ow!",
            "Agh!"
        };

        SubtitlesUI.Get().AddText(randomMessages0[Random.Range(0, randomMessages0.Length)], 5);

        float minYPositionDeath = transform.position.y - 0.3f;

        // Repeat until one second has passed
        float currentTime = 0;
        while (currentTime < 1)
        {
            currentTime += Time.deltaTime;

            Vector3 wantedDir = character.lookTarget.position - transform.position;
            wantedDir.y = 0;
            transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(wantedDir), Time.deltaTime * 4);

            transform.position -= transform.forward * Time.deltaTime;

            character.playAnimation("shove_B", 0.25f);

            if (transform.position.y < minYPositionDeath)
            {
                character.status = characterStatus.Unconsious;
                character.enableRagdoll();
                character.playFaceAnimation("Face_Sleep", 0.5f);
                character.ragdoll.Limbs["Spine_2"].GetComponent<Rigidbody>().velocity = -transform.forward * 15;
                Scream();
                break;
            }

            yield return new WaitForEndOfFrame();
        }

        if (!character.ragdoll.ragdolled)
        {
            HeadLook.lookAtPoint = player.references.Head;

            if (timesShoved < 5)
            {
                #region Stop being shoved & go back to where the student was
                character.playAnimation("Investigate", 0.25f);
                character.status = originalStatus;
                character.lookAt(origin);
                Destroy(empty);

                yield return new WaitForSeconds(1);

                if (character.status == characterStatus.BeingShoved) yield break;

                if (origin == player.transform)
                {
                    playerOpinion -= 0.15f;
                }

                string[] randomMessages1 = new string[]
                {
                    "Watch where you're going!",
                    "What the hell!",
                    "Jeez, watch it!",
                    "Ugh! Watch where you're going!",
                    "Seriously? Ugh.",
                    "Slow down!",
                    "This is why you're not allowed to run on schoolgrounds...",
                    "Don't run! Jeez!",
                    "Are you late for something? Calm down..."
                };

                SubtitlesUI.Get().AddText(randomMessages1[Random.Range(0, randomMessages1.Length)], 5);

                yield return new WaitForSeconds(2);

                // Reset brows
                character.faceMesh.SetBlendShapeWeight(2, 0);

                if (character.status == originalStatus)
                {
                    character.lookAt(null);
                    character.resumeMovement();
                    character.setDestination(character.currentDestination, character.running);
                }
                #endregion
            }
            else
            {
                #region Leave the area
                character.playAnimation(character.movementAnimation.idleAnim, 0.25f);
                character.status = originalStatus;
                character.lookAt(origin);
                Destroy(empty);

                yield return new WaitForSeconds(1);

                if (character.status == characterStatus.BeingShoved) yield break;

                string[] randomMessages1 = new string[]
                {
                    "Fine. I can tell when I'm not wanted here.",
                    "I can take a hint. I'm off.",
                    "Fine. I'll leave.",
                    "There are better ways of saying I'm not needed here...",
                    "Guess I'm not wanted here.",
                    "Guess I'm not needed here."
                };
                playerOpinion = 0;
                SubtitlesUI.Get().AddText(randomMessages1[Random.Range(0, randomMessages1.Length)], 5);

                // Reset brows
                character.faceMesh.SetBlendShapeWeight(2, 0);

                if (character.status == originalStatus)
                {
                    character.lookAt(null);

                    Destination newSpot = studentManager.goAwaySpots[Random.Range(0, studentManager.goAwaySpots.Length)];

                    // Only go to the new spot if its more than 20 meters away from the original spot
                    while (Operators.Distance(newSpot.transform.position, transform.position) < 20)
                    {
                        newSpot = studentManager.goAwaySpots[Random.Range(0, studentManager.goAwaySpots.Length)];
                    }

                    character.setDestination(newSpot, false);
                    character.resumeMovement();

                    yield return new WaitForSeconds(5);

                    timesShoved = 0;
                }
                #endregion
            }

            HeadLook.lookAtPoint = null;
        }
    }

    // Spawn a random subtitle message
    void addRandomSubtitle(string[] text, float stayTime)
    {
        int randomText = Random.Range(0, text.Length);
        SubtitlesUI.Get().AddText(text[randomText], stayTime);
    }

    public float GetPathLength(NavMeshPath path)
    {
        float lng = 0.0f;

        if ((path.status != NavMeshPathStatus.PathInvalid) && (path.corners.Length > 1))
        {
            for (int i = 1; i < path.corners.Length; ++i)
            {
                lng += Operators.Distance(path.corners[i - 1], path.corners[i]);
            }
        }

        return lng;
    }

    // Be alerted to a scream
    IEnumerator noticeScream(Student me)
    {
        if (character.status == characterStatus.Normal)
        {
            // Look at the direction of the scream
            Destination lastSpot = character.currentDestination;
            character.status = characterStatus.Suspicious;
            character.pauseMovement();
            character.lookAt(me.transform);
            HeadLook.lookAtPoint = me.Head;
            character.playAnimation("Investigate", 0.25f);
            emote(studentEmotes.Suspicion);

            yield return new WaitForSeconds(Random.Range(3f, 7f));

            HeadLook.lookAtPoint = null;
            bool decideToGoCheck = Random.Range(0, 2) == 0 ? false : true;

            if (decideToGoCheck)
            {
                GameObject newSpot = new GameObject("InvestigateSpot");
                Destination spot = newSpot.AddComponent<Destination>();
                spot.transform.position = me.transform.position;

                character.resumeMovement();
                NavMeshPath newPath = new NavMeshPath();
                character.movementAgent.CalculatePath(spot.transform.position, newPath);

                float pathLength = GetPathLength(newPath);
                if (pathLength < 15)
                {
                    character.lookAt(null);
                    // Go to the scream if there's nothing there
                    if (character.status == characterStatus.Suspicious)
                    {
                        // Create a new destination at the area of the scream and go and see what's there
                        character.resumeMovement();
                        character.setDestination(spot, false);
                        yield return new WaitUntil(() => character.arrivedAtDestination);
                        // Look around for a bit at the spot the scream was at
                        character.pauseMovement();
                        character.playAnimation(character.movementAnimation.idleAnim, 0.25f);
                        spot.transform.eulerAngles = transform.eulerAngles;
                        yield return new WaitForSeconds(Random.Range(3f, 5f));
                        // Go back to the last spot they were at if there's nothing there
                        if (!character.ragdoll.ragdolled)
                        {
                            character.resumeMovement();
                            character.status = characterStatus.Normal;
                            character.setDestination(lastSpot, false);
                        }
                    }
                }
                Destroy(newSpot);
            }
            else
            {
                if (!character.ragdoll.ragdolled)
                {
                    character.lookAt(null);
                    character.resumeMovement();
                    character.status = characterStatus.Normal;
                    character.setDestination(lastSpot, false);
                }
            }
        }
    }
}