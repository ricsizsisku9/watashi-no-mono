﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TalkUI : MonoBehaviour {
    #region References
    [SerializeField] Animator UIAnimator;
    [SerializeField] Animator UIChoiceAnimator;
    [SerializeField] talkSelectionChoice[] selectionChoices;
    [SerializeField] Text studentText;
    [SerializeField] Text studentName;
    [SerializeField] bool choicesActive;
    [SerializeField] RectTransform selector;
    [SerializeField] AudioSource audioSRC;
    [SerializeField] PlayerController player;
    [SerializeField] string currentText;
    [SerializeField] Image fillImage;
    [SerializeField] Slider opinionSlider;
    [SerializeField] Image opinionSliderFace;
    [SerializeField] Sprite[] opinionSliderFaces;
    public Student selectedStudent;
    public bool active;
    public int currentSelected;

    int availableOptions;
    float cooldown;
    public bool playerInputted;
    bool waitingForInput;
    public bool fillDecay = true;

    Dictionary<string, playerTalkResponses> responseType =
            new Dictionary<string, playerTalkResponses>()
            {
                { "Sorry, nevermind.",          playerTalkResponses.Leave },
                { "You look great today!",      playerTalkResponses.Compliment },
                { "I'm Uchikina.",              playerTalkResponses.Meet },
                { "Can I ask you something?",   playerTalkResponses.AskMore },
                { "Can you follow me?",         playerTalkResponses.Follow },
                { "Can I give you something?",  playerTalkResponses.Gift },
                { "Do you need anything?",      playerTalkResponses.Task}
            };
    #endregion

    public void Update()
    {
        updateSelector();
        if (active)
        {
            updateControls();
            updateOpinion();
            if (fillDecay)
            {
                fillImage.fillAmount -= Time.deltaTime / 25;
            }
            if (fillImage.fillAmount == 0)
            {
                player.stopTalking();
                SubtitlesUI.Get().AddText("I gotta go. Sorry.", 5f);
            }
        }
    }

    void updateOpinion()
    {
        if (selectedStudent != null)
        {
            opinionSlider.value = Mathf.Lerp(opinionSlider.value, selectedStudent.playerOpinion, Time.deltaTime * 8);
            if (opinionSlider.value <= 0.6f)
            {
                opinionSliderFace.sprite = opinionSliderFaces[0];
            }
            if (opinionSlider.value > 0.6f && opinionSlider.value < 1.5f)
            {
                opinionSliderFace.sprite = opinionSliderFaces[1];
            }
            if (opinionSlider.value >= 1.5f)
            {
                opinionSliderFace.sprite = opinionSliderFaces[2];
            }
        }
    }

    void updateSelector()
    {
        selector.position = Vector3.Lerp(selector.position, selectionChoices[currentSelected].selectSpot.position, Time.deltaTime * 10);
        selector.eulerAngles = Vector3.Lerp(selector.eulerAngles, selectionChoices[currentSelected].selectSpot.eulerAngles, Time.deltaTime * 10);
    }

    void updateControls()
    {
        #region Button Movement
        if (choicesActive)
        {
            float vertical = Mathf.Clamp(Input.GetAxis("Vertical"), -0.01f, 0.01f);
            if (cooldown == 0)
            {
                if (vertical > 0)
                {
                    cooldown = player.stats.keyboard ? 0.15f : 0.25f;
                    currentSelected++;
                    audioSRC.PlayOneShot(audioSRC.clip);
                    if (currentSelected > availableOptions)
                        currentSelected = 0;
                }
                else if (vertical < 0)
                {
                    cooldown = player.stats.keyboard ? 0.15f : 0.25f;
                    currentSelected--;
                    audioSRC.PlayOneShot(audioSRC.clip);
                    if (currentSelected < 0)
                        currentSelected = availableOptions;
                }
            }
            if (cooldown > 0)
                cooldown -= Time.deltaTime;
            else
                cooldown = 0;
        }
        #endregion

        #region Interaction
        if (Input.GetButtonDown("Interact"))
        {
            audioSRC.PlayOneShot(audioSRC.clip);
            if (!waitingForInput)
            {
                if (choicesActive)
                    initiateResponse(selectionChoices[currentSelected].respondType);
            }
            else
            {
                playerInputted = true;
            }
        }
        if (Input.GetButtonUp("Interact"))
        {
            if (playerInputted)
                playerInputted = false;
        }
        #endregion
    }

    void initiateResponse(playerTalkResponses responseType)
    {
        StartCoroutine(interact(responseType));
    }

    public IEnumerator interact(playerTalkResponses responseType)
    {
        Student student = selectedStudent;
        switch (responseType)
        {
            case playerTalkResponses.Leave:
                #region End the conversation
                // The conversation will end.
                player.stopTalking();
                #endregion
                break;
            case playerTalkResponses.Compliment:
                #region Compliment the student
                // Uchikina will now speak.
                fillDecay = false;
                resetCurrentAnimation();
                playAnimation("LookDown", 0.25f);
                hideChoices();
                setName("Uchikina Kurutta");
                randomMessage(new string[]
                {
                    "I just wanted to say how great you look today!",
                    "You look really good today!",
                    "Have you always worn your uniform like that? You look really nice!",
                    "You look great today!"
                });
                waitForInput(); yield return new WaitUntil(() => playerInputted == true);
                // The student will now speak.
                student.complimented++;
                setStudentName(student);
                if (student.complimented == 1)
                {
                    playStudentAnimation("Bow", 0.25f);
                    editOpinion(student, 0.15f);
                    randomMessage(student.complimentResponses);
                    student.emote(studentEmotes.OpinionPlus);
                }
                else if (student.complimented == 2)
                {
                    editOpinion(student, -0.05f);
                    playStudentAnimation("LookAway", 0.25f);
                    setText("Didn't you just say that?");
                    student.emote(studentEmotes.Suspicion);
                }
                else if (student.complimented == 3)
                {
                    editOpinion(student, -0.05f);
                    playStudentAnimation("LookAway", 0.25f);
                    setText("You just said that...");
                    student.emote(studentEmotes.OpinionMinus);
                }
                else
                {
                    editOpinion(student, -0.15f);
                    playStudentAnimation("LookAway", 0.25f);
                    randomMessage(new string[]
                    {
                        "You can stop now.",
                        "Quit it.",
                        "Cut it out.",
                    });
                    student.emote(studentEmotes.OpinionMinus);
                }
                waitForInput(); yield return new WaitUntil(() => playerInputted == true);
                activate(null);
                setChoices("Sorry, nevermind.", "You look great today!", "I'm Uchikina.", "Can I ask you something?", null);
                #endregion
                break;
            case playerTalkResponses.Meet:
                #region Meet the student
                // Uchikina will now speak.
                fillDecay = false;
                resetCurrentAnimation();
                playAnimation("Bow", 0.25f);
                hideChoices();
                setName("Uchikina Kurutta");
                randomMessage(new string[]
                {
                    "I don't think we've met. I'm Uchikina!",
                    "I'm Uchikina! Nice to meet you!",
                    "I don't think i've met you yet. I'm Uchikina!",
                    "I've never met you yet. I'm Uchikina!"
                });
                waitForInput(); yield return new WaitUntil(() => playerInputted == true);
                // The student will now speak.
                setStudentName(student);
                if (!student.metPlayer)
                {
                    student.metPlayer = true;
                    setStudentName(student);
                    playStudentAnimation("Bow", 0.25f);
                    randomMessage(student.meetMessages);
                }
                else
                {
                    editOpinion(student, -0.07f);
                    playStudentAnimation("LookDown", 0.25f);
                    randomMessage(new string[]
                    {
                        "We've... already met...",
                        "Um... We know eachother...",
                        "Haven't we already met?",
                    });
                    student.emote(studentEmotes.Suspicion);
                }
                waitForInput(); yield return new WaitUntil(() => playerInputted == true);
                activate(null);
                setChoices("Sorry, nevermind.", "You look great today!", "I'm Uchikina.", "Can I ask you something?", null);
                #endregion
                break;
            case playerTalkResponses.AskMore:
                #region Give more options
                currentSelected = 0;
                randomMessage(new string[]
                {
                    "Yeah?",
                    "What is it?",
                    "What?",
                    "What's up?",
                    "What do you need?"
                });
                setChoices("Sorry, nevermind.", "Can you follow me?", "Can I give you something?", "Do you need anything?", null);
                #endregion
                break;
            case playerTalkResponses.Follow:
                #region Ask to be followed
                fillDecay = false;
                resetCurrentAnimation();
                playAnimation("Wave", 0.25f);
                hideChoices();
                setName("Uchikina Kurutta");
                setText("Hey, come with me! I have a surprise for you!");
                waitForInput(); yield return new WaitUntil(() => playerInputted == true);
                setStudentName(student);
                setText("Sorry, I don't know you.");
                student.emote(studentEmotes.Suspicion);
                waitForInput(); yield return new WaitUntil(() => playerInputted == true);
                player.stopTalking();
                SubtitlesUI.Get().AddText("THIS IS A TEST : Student is now following you.", 5f);
                student.character.follow(player.transform, followType.RunAndWalk);
                //activate(null);
                //setChoices("Sorry, nevermind.", "You look great today!", "I'm Uchikina.", "Can I ask you something?", null);
                #endregion
                break;
            case playerTalkResponses.Gift:
                #region Ask to be followed
                fillDecay = false;
                resetCurrentAnimation();
                playAnimation("Wave", 0.25f);
                hideChoices();
                setName("Uchikina Kurutta");
                setText("I have a gift for you!");
                waitForInput(); yield return new WaitUntil(() => playerInputted == true);
                player.showUI = true;
                setText(" ");
                deactivate();
                playerInputted = false;
                yield return new WaitForSeconds(1);
                waitForInput(); yield return new WaitUntil(() => playerInputted == true);
                float fillAmount = fillImage.fillAmount;
                player.showUI = false;
                student.emote(studentEmotes.OpinionPlus);
                setText("Here!");
                activate(null);
                waitForInput(); yield return new WaitUntil(() => playerInputted == true);
                setChoices("Sorry, nevermind.", "You look great today!", "I'm Uchikina.", "Can I ask you something?", null);
                #endregion
                break;
        }
    }

    public void randomMessage(string[] random)
    {
        int randomMessage = Random.Range(0, random.Length);
        setText(random[randomMessage]);
    }

    public void waitForInput()
    {
        playerInputted = false;
        waitingForInput = true;
    }

    public void activate(string name)
    {
        UIAnimator.Play("Open");
        if (name == null)
        {
            setStudentName(selectedStudent);
        }
        else
        {
            setName(name);
        }
        currentSelected = 0;
        fillDecay = true;
        active = true;
        waitingForInput = false;
        playerInputted = false;
        fillImage.fillAmount = 1f;
        if (selectedStudent != null)
        {
            opinionSlider.value = selectedStudent.playerOpinion;
        }
    }

    public void deactivate()
    {
        UIAnimator.Play("Close");
        currentSelected = 0;
        active = false;
        UIChoiceAnimator.Play("Close");
        choicesActive = false;
    }

    public void setText(string text)
    {
        StartCoroutine(typeText(text));
    }

    void setName(string name)
    {
        studentName.text = name;
    }

    void setStudentName(Student student)
    {
        if (student)
        {
            if (student.metPlayer)
            {
                studentName.text = student.name;
            }
            else
            {
                studentName.text = "??????";
            }
        }
    }

    void playAnimation(string clipName, float transitionTime)
    {
        if (player.stats.currentAnimation != player.moveAnims.sitAnim)
        {
            Debug.Log(player.stats.currentAnimation);
            Debug.Log(clipName);
            player.playAnimation(clipName, transitionTime);
        }
    }

    void playStudentAnimation(string clipName, float transitionTime)
    {
        if (selectedStudent.character.currentAnimation != selectedStudent.character.movementAnimation.sitAnim)
        {
            selectedStudent.character.playAnimation(clipName, transitionTime);
        }
    }

    void resetCurrentAnimation()
    {
        if (player.stats.currentAnimation != player.moveAnims.sitAnim)
        {
            player.stats.currentAnimation = "";
        }
    }

    public void editOpinion(Student student, float difference)
    {
        student.playerOpinion += difference;
    }

    public void setChoices(string option1Text, string option2Text, string option3Text, string option4Text, string option5Text)
    {
        choicesActive = true;
        UIChoiceAnimator.Play("Open");
        currentSelected = 0;
        availableOptions = -1;
        updateChoice(0, option1Text);
        updateChoice(1, option2Text);
        updateChoice(2, option3Text);
        updateChoice(3, option4Text);
        updateChoice(4, option5Text);
    }

    public void hideChoices()
    {
        choicesActive = false;
        UIChoiceAnimator.Play("Close");
    }

    void updateChoice(int number, string text)
    {
        if (text != null)
        {
            selectionChoices[number].myImage.enabled = true;
            if (responseType.ContainsKey(text))
                selectionChoices[number].respondType = responseType[text];
            selectionChoices[number].myText.text = text;
            availableOptions++;
        }
        else
        {
            selectionChoices[number].myImage.enabled = false;
            selectionChoices[number].myText.text = "";
        }
    }

    IEnumerator typeText(string text1)
    {
        string text = text1.Replace("%studentname%", studentName.text);
        currentText = text;
        int currentLetter = 0;

        while (currentLetter <= text.Length)
        {
            if (currentLetter == text.Length)
                break;
            if (currentText != text)
            {
                currentLetter = text.Length;
                break;
            }
            else
            {
                currentLetter++;
                studentText.text = text.Substring(0, currentLetter);
            }
            yield return new WaitForEndOfFrame();
        }
    }
}
