﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AI;
using UnityEngine.Events;

//////////////////////
////// Classes ///////
//////////////////////

// Customizable events that happen throughout the day
[Serializable] public class WNM_Event
{
    public string eventName = "New Day Event";
    public int hour = 7;
    public int minute = 0;
}

// Universal movement information for NPCs & the player
[Serializable] public class movementInfo
{
    public float crawlSpeed = 0.8f;
    public float walkSpeed = 1.0f;
    public float runSpeed = 3.0f;
    public float sprintSpeed = 5.0f;
    public float turnSpeed = 8.0f;
}

// Universal movement information for NPCs & the player
[Serializable] public class movementAnimations
{
    public string idleAnim = "defaultIdle";
    public string walkAnim = "defaultWalk";
    public string runAnim = "defaultRun";
    public string sprintAnim = "defaultSprint";
    public string sitAnim = "sit_Begin";
    public string crouchAnim = "defaultCrouch";
    public string crawlAnim = "defaultCrawl";
}

// Current player status variables
[Serializable] public class playerInfo
{
    [Header("Current Variables")]
    public playerMovementMode canMove = playerMovementMode.all;
    public string currentAnimation;
    public string currentItemAnimation;
    public bool promptsEnabled = true;
    public bool keyboard = true;
    public playerStatus playerState;
    public Student selectedStudent;
    public int itemEquipped;
    public float sanity = 4;
    public int bloodiness = 0;
    public clothingType currentClothing;
    public Student currentStudentEavesDropping;

    [Header("Stats")]
    public float strength = 1;

    public bool holdingWeapon()
    {
        List<int> dangerousWeapons = new List<int>() { 1, 3 };
        return dangerousWeapons.Contains(itemEquipped);
    }

    public bool canMovePlayer()
    {
        return canMove == playerMovementMode.all || canMove == playerMovementMode.playerOnly;
    }

    public bool canMoveCamera()
    {
        return canMove == playerMovementMode.all || canMove == playerMovementMode.cameraOnly;
    }
}

[Serializable] public class ragdollInfo
{
    public Dictionary<string, Transform> Limbs;
    public bool ragdolled;
    Rigidbody[] ragdollBodies;
    Collider[] ragdollColliders;
    CharacterJoint[] ragdollJoints;

    public void findRagdollComponents(Transform character)
    {
        SkinnedMeshRenderer baseMesh = null;
        foreach (SkinnedMeshRenderer mesh in character.GetComponentsInChildren<SkinnedMeshRenderer>())
        {
            // Detect the mesh as the character's base mesh if it has more than 5 bones
            if (mesh.bones.Length > 5)
            {
                baseMesh = mesh;
                break;
            }
        }

        // Stop the void if there isn't a basemesh
        if (baseMesh == null) return;

        ragdollBodies = character.GetComponentsInChildren<Rigidbody>();
        ragdollColliders = character.GetComponentsInChildren<Collider>();
        ragdollJoints = character.GetComponentsInChildren<CharacterJoint>();
        Limbs = new Dictionary<string, Transform>();
        Transform[] tempLimbs = baseMesh.bones;
        foreach (Transform limb in tempLimbs)
        {
            Limbs.Add(limb.name, limb);
        }
    }

    public void enable()
    {
        ragdolled = true;

        foreach (Rigidbody body in ragdollBodies)
        {
            body.isKinematic = false;
            body.mass = 2f;
        }

        foreach (Collider collider in ragdollColliders)
        {
            collider.enabled = true;
        }

        foreach (CharacterJoint joint in ragdollJoints)
        {
            joint.enableProjection = false;
            joint.enablePreprocessing = false;
        }
    }

    public void disable()
    {
        ragdolled = false;

        foreach (Rigidbody body in ragdollBodies)
        {
            body.isKinematic = true;
        }

        foreach (Collider collider in ragdollColliders)
        {
            collider.enabled = false;
        }
    }
}

// Information that a prompt reads from
[Serializable] public class promptInfo
{
    public promptType renderType;
    public float fillSpeed = 2f;
    public UnityEvent OnFill;
    public UnityEvent OnFillStart;
    public UnityEvent OnFillEnd;
    public bool fillAfterFinished;
    public Vector3 promptOffset = Vector3.zero;
    public string promptText = "PROMPT_TEXT";
    public string promptKey = "Interact";
}

// References for the player are a class to keep the main code clean and neat while all the bulky references are below.
[Serializable] public class playerReferences
{
    [Header("Player")]
    public CapsuleCollider collider;
    public Rigidbody rigidbody;
    public CharacterController controller;
    public PlayerCamera mainCamera;
    public Animator animator;
    public Animator faceAnimator;
    public AudioSource audioSource;
    public Transform rightHandItemParent;
    public Transform Spine2;
    public Transform dragPosition;
    public Transform Head;
    public Transform Hips;
    public GameObject[] uniformPrefabs;
    public Material[] bodyTextures;
    public SkinnedMeshRenderer clothingMesh;
    public SkinnedMeshRenderer skirtMesh;
    public Mesh[] clothingMeshes;
    public Mesh[] lowPolyClothingMeshes;
    public Material[] defaultClothingMaterials;
    public Material[] gymClothingMaterials;
    public Projector bloodProjector;
    public ClothingLocker playerLocker;
    public LayerMask worldLayers;
    [Header("UI/Camera")]
    public CanvasGroup mainUICanvas;
    public Transform[] cameraPositions;
    public Vector3[] attackPositionOffsets;
    public Animator eavesdropUIAnimator;
    public Text eavesdropText;
    [Header("Assets")]
    public Material[] bloodMaterials;
    public Material[] studentDamageMaterials;
    [Header("Managers")]
    public PromptManager promptManager;
    public AttackManager attackManager;
    public PoliceManager policeManager;
    public ScreenFade fadeManager;
    public Incinerator incinerator;
    public Inventory inventoryManager;
    public TalkUI talkManager;
    public TimeManager timeManager;
    public HeadLookAt headLook;
}

// Game Settings
[Serializable] public class gameSettings
{
    // Input
    public float mouseSensitivity = 8f;
    public float playerScaleTest = 1f;
    public float studentScaleMultiplier = 1f;
    public float musicVolume = 0.5f;
    public float voiceVolume = 1f;
    public float effectVolume = 1f;
    public float cameraFar = 1000f;
    public int textureDivide = 0;
    public bool shadowsEnabled = true;
    public bool enableUI = true;
    public bool imageEffects = true;
    public bool sunEnabled = true;
    public bool particlesEnabled = true;
    public bool animationsEnabled = true;
    public bool debugHotkeysEnabled = false;
    public bool debugConsoleEnabled = false;
    public bool importPackages = false;
}

// Sanity & Low sanity versions of a schoolday track
[Serializable] public class schooldayTrack
{
    public AudioClip saneAudio;
    public AudioClip insaneAudio;
}

// References for the talk UI choices
[Serializable] public class talkSelectionChoice
{
    public Text myText;
    public Image myImage;
    public RectTransform selectSpot;
    public playerTalkResponses respondType;
}

// Customizable Scripted Events
[Serializable] public class scriptedEvent
{
    public string eventName = "New Scripted Event";

    [Header("What time the event will happen")]
    public time eventTime = new time() { hour = 7, minute = 0, PM = false };

    [Header("Phases in the event")]
    public eventPhase[] Phases;

    [Header("Which students the event will wait for before starting")]
    public eventNecessaryStudents[] necessaryStudents;
}

// Phases in an event
[Serializable] public class eventPhase
{
    public string subtitleText;
    public AudioClip voicedLine;
    public float voicedLineVolume = 1f;

    public Student highlightedStudent;

    public Destination wantedDestination;
    public bool runToDestination;

    public eventAnimation[] AnimationsToPlay;

    public float secondsToWait;

    public UnityEvent specialFunctions = new UnityEvent();
}

// A class to control what animation plays on what student in a scripted event
[Serializable]
public class eventAnimation
{
    public Student highlightedStudent;
    public studentConversationAnimation animation;
    public Destination wantedDestinationWithoutPhaseDelay;
    public bool runToDestination;
}

// Places where the students will go at the end of the event
[Serializable] public class eventNecessaryStudents
{
    public Student highlightedStudent;
    public bool dontWaitForStudent;

    [Header("Set the student to be busy and refuse to talk to the player or not")]
    public studentEventBusyType setStudentToBusy;

    [Header("Where the student will go for the event to start")]
    public Destination wantedDestination;
    public bool runToDestination;

    [Header("Where the student will go after the event")]
    public Destination endDestination;
    public bool runToEndDestination;
}

// Hour & Minute to read/display
[Serializable] public class time
{
    public int hour;
    public int minute;
    public bool PM;

    public bool MatchTime(time otherTime)
    {
        if (otherTime.hour == hour && otherTime.minute == minute && otherTime.PM == PM)
            return true;
        else
            return false;
    }
}

[Serializable] public class policeEvidenceItem
{
    public string name;
    public evidenceType type;
    public Vector3 position;
}

[Serializable] public class saveData
{
    public string saveName = "Save File";

    public List<policeEvidenceItem> evidence = new List<policeEvidenceItem>();
    public List<string> absentStudents = new List<string>();
    public List<string> studentsMet = new List<string>();
    public int studentsKilled;
    public float playerStrength;
    public float playerSpeed;
    public float playerSeduction;
    public int am;
    public int playerTired;
    public int week;
    public int day;
}

//////////////////////
/////// Enums ////////
//////////////////////

[Serializable] public enum characterStatus
{
    Normal,
    Suspicious,
    Scared,
    Dead,
    Unconsious,
    Talking,
    Attacking,
    Dragging,
    Sitting,
    Eavesdropping,
    Crouching,
    BeingShoved
}

[Serializable] public enum followType
{
    RunAndWalk,
    WalkOnly,
    Chase
}

[Serializable] public enum saveStatus
{
    Working,
    Nonexistant,
    Corrupt
}

[Serializable] public enum evidenceType
{
    item,
    body,
    uniform
}

// Ways a student can be set to busy
[Serializable] public enum studentEventBusyType
{
    DontSetToBusy,
    BusyOnEvent,
    BusyOnWalkingToEvent
}

// Animations the student can do in a conversation
[Serializable] public enum studentConversationAnimation
{
    Salute0,
    Greet0,
    Greet1,
    Think0,
    Shake0,
    Nod0
}

// Self Explanatory
[Serializable] public enum Gender
{
    Male,
    Female
}

// Types of clothing the player or students can wear
[Serializable] public enum clothingType
{
    Default,
    Gym,
    None
}

// Types of movement a camera can have
[Serializable] public enum cameraType
{
    Default,
    FreeFlying
}

// Types of ways the player can be suspicious
[Serializable] public enum playerSuspicionTypes
{
    normal,
    bloody,
    insane,
    hasSuspiciousItem
}

// Student behavior types
[Serializable] public enum Persona
{
    Default,
    Heroic
}

// States the player have
[Serializable] public enum playerStatus
{
    Nothing,
    Talking,
    Attacking,
    Dragging,
    Sitting,
    Eavesdropping,
    Crouching
}

// Ways a prompt's sprite will change
[Serializable] public enum promptType
{
    Normal,
    Dangerous
}

// Player movement modes
[Serializable] public enum playerMovementMode
{
    all,
    cameraOnly,
    playerOnly,
    none
}

// Types the way an item will be picked up
[Serializable] public enum weaponType
{
    SmallItem,
    SmallWeapon,
    LargeItem,
    LargeWeapon
}

// Actions students will do when they arrive at their destination
[Serializable] public enum DestinationAction
{
    Stand,
    Sit,
    LookAt,
    TalkSolo,
    Disappear
}

// Ways a door can be opened
[Serializable] public enum doorType
{
    SwingingDoubleDoor,
    SingleSwingingDoor,
    SlidingDoubleDoor,
    SingleSlidingDoor,
    DEPRECATEDDumpsterDoor,
    BathroomStallDoor
}

// All the ways a student can react to the player
[Serializable] public enum playerReactionTypes
{
    NoReaction,
    Murder,
    Suspicious
}

// Types of emotes a student can emit
[Serializable] public enum studentEmotes
{
    Scare,
    OpinionPlus,
    OpinionMinus,
    Suspicion
}

// Ways the player can respond
[Serializable] public enum playerTalkResponses
{
    Leave,
    Compliment,
    Meet,
    AskMore,
    Follow,
    Gift,
    Task,
    INTERROGATION1,
    INTERROGATION2,
    INTERROGATION3,
    INTERROGATION4,
    INTERROGATION5
}