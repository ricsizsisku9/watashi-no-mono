﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseMenu : MonoBehaviour {
    public Animator pauseMenuAnimator;
    public PlayerController player;

    void Start()
    {
        PlayerPrefs.SetInt("GamePaused", 0);
    }

    void Update () {
	    if (Input.GetButtonDown("Pause"))
        {
            if (player.stats.playerState == playerStatus.Nothing)
            {
                if (PlayerPrefs.GetInt("GamePaused") == 0)
                {
                    PlayerPrefs.SetInt("GamePaused", 1);
                    pauseMenuAnimator.Play("Open");
                    player.stats.canMove = playerMovementMode.none;
                    player.playAnimation("Texting", 0.2f);
                    player.references.timeManager.wantedTimeScale = 0f;
                }
                else
                {
                    PlayerPrefs.SetInt("GamePaused", 0);
                    pauseMenuAnimator.Play("Close");
                    player.stats.canMove = playerMovementMode.all;
                    player.playAnimation(player.moveAnims.idleAnim, 0.2f);
                    player.references.timeManager.wantedTimeScale = 1f;
                }
            }
        }
	}
}
