﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScreenFade : MonoBehaviour {
    [SerializeField] Image fadeImage;
    public CanvasGroup alphaGroup;
    public Color fadeColor;
    public float fadeSpeed = 8;
    public bool fade;

	void Update () {
        fadeImage.color = fadeColor;
        if (fade)
        {
            alphaGroup.alpha = Mathf.Lerp(alphaGroup.alpha, 1.1f, Time.deltaTime * fadeSpeed);
        }
        else
        {
            alphaGroup.alpha = Mathf.Lerp(alphaGroup.alpha, -0.1f, Time.deltaTime * fadeSpeed);
        }
    }
}
