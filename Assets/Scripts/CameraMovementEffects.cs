﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovementEffects : MonoBehaviour {
    [SerializeField] PlayerController player;
    [SerializeField] Vector3 noise;
    public float shakeMultiplier = 1.0f;
    public float shakeSpeed = 1.0f;
    Vector3 noiseOffset;

    void LateUpdate()
    {
        if (player.stats.canMoveCamera() && player.references.mainCamera.Type == cameraType.Default)
        {
            float noiseOffsetDelta = Time.deltaTime * shakeSpeed;

            noiseOffset.x += noiseOffsetDelta;
            noiseOffset.y += noiseOffsetDelta;
            noiseOffset.z += noiseOffsetDelta;

            noise.x = Mathf.PerlinNoise(noiseOffset.x, 0.0f);
            noise.y = Mathf.PerlinNoise(noiseOffset.y, 1.0f);
            noise.z = Mathf.PerlinNoise(noiseOffset.z, 2.0f);

            player.references.mainCamera.transform.position += new Vector3(noise.x * shakeMultiplier, noise.y * shakeMultiplier, noise.z * shakeMultiplier);
        }
    }
}
